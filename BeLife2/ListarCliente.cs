﻿using Conexion;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Modelo.Clases;
using System;
using System.Collections.Generic;

namespace Vista
{
    public partial class ListarCliente : MetroForm
    {
        private String Rut { get; set; }
        private Int32 IdSexo { get; set; }
        private Int32 IdEstado { get; set; }
        private AgregarCliente ParentWindow { get; set; }

        #region Constructor ListarCliente()
        public ListarCliente()
        {
            InitializeComponent();

            ComboModo.Items.Add("Filtrar por RUT");
            ComboModo.Items.Add("Filtrar por Sexo");
            ComboModo.Items.Add("Filtrar por Estado Civil");
            ComboModo.SelectedIndex = 0;

            BtnCargar.Enabled = false;
            BtnCargar.Visible = false;

            LlenarDatos();
        }
        #endregion

        #region MostrarBotonCargar(AgregarCliente)
        public void MostrarBotonCargar(AgregarCliente Ventana)
        {
            BtnCargar.Enabled = true;
            BtnCargar.Visible = true;

            ParentWindow = Ventana;
        }
        #endregion

        #region LlenarDatos()
        private void LlenarDatos()
        {
            GridLista.Rows.Clear();

            DAOCliente Clientes = new DAOCliente();

            List<Cliente> Lista = Clientes.GetClientes(Rut, IdSexo, IdEstado);

            if (Lista.Count == 0) { return; }

            Lista.ForEach(delegate (Cliente C)
            {
                String EstadoCivil = new DAOEstadoCivil().GetEstadoCivil(C.IdEstadoCivil).Descripcion;
                String Sexo = new DAOSexo().GetSexo(C.IdSexo).Descripcion;

                List<String> Fila = new List<String>
                {
                    C.RutCliente,
                    C.Nombres,
                    C.Apellidos,
                    C.FechaNacimiento,
                    Sexo,
                    EstadoCivil
                };

                GridLista.Rows.Add(Fila.ToArray());
            });
        }
        #endregion

        #region Selection Changed ComboModo
        private void ComboModo_SelectedIndexChanged(Object sender, EventArgs e)
        {
            Int32 Modo = (sender as MetroComboBox).SelectedIndex;

            ComboFiltro.Enabled = Modo > 0;
            ComboFiltro.Visible = Modo > 0;
            TextFiltro.Enabled = Modo == 0;
            TextFiltro.Visible = Modo == 0;

            TextFiltro.Text = "";

            if (Modo == 0) { return; }

            ComboFiltro.Items.Clear();
            ComboFiltro.Items.Add("Sin Filtro");
            ComboFiltro.SelectedIndex = 0;
            switch (Modo)
            {
                case 1:
                    DAOSexo Sexos = new DAOSexo();
                    Sexos.GetAllSexos().ForEach(delegate (Sexo S) { ComboFiltro.Items.Add(S.Descripcion); });
                break;
                case 2:
                    DAOEstadoCivil Estados = new DAOEstadoCivil();
                    Estados.GetAllEstadoCivil().ForEach(delegate (EstadoCivil E) { ComboFiltro.Items.Add(E.Descripcion); });
                break;
            }
        }
        #endregion

        #region Text Changed TextFiltro
        private void TextFiltro_TextChanged(Object sender, EventArgs e)
        {
            Rut = (sender as MetroTextBox).Text;
            LlenarDatos();
        }
        #endregion

        #region Selection Changed ComboFiltro
        private void ComboFiltro_SelectedIndexChanged(Object sender, EventArgs e)
        {
            Int32 Sel = (sender as MetroComboBox).SelectedIndex;
            Int32 Modo = ComboModo.SelectedIndex;

            switch (Modo)
            {
                case 1:
                    IdSexo = Sel;
                    IdEstado = 0;
                break;
                case 2:
                    IdSexo = 0;
                    IdEstado = Sel;
                break;
            }
            LlenarDatos();
        }
        #endregion

        #region Click BtnCargar
        private void BtnCargar_Click(Object sender, EventArgs e)
        {
            if (GridLista.SelectedCells.Count == 0) { return; }

            DAOSexo Sexos = new DAOSexo();
            DAOEstadoCivil Estados = new DAOEstadoCivil();

            Cliente LoadCliente = new Cliente
            {
                RutCliente = GridLista.SelectedCells[0].Value.ToString(),
                Nombres = GridLista.SelectedCells[1].Value.ToString(),
                Apellidos = GridLista.SelectedCells[2].Value.ToString(),
                FechaNacimiento = GridLista.SelectedCells[3].Value.ToString(),
                IdSexo = Sexos.GetSexo(GridLista.SelectedCells[4].Value.ToString()).IdSexo,
                IdEstadoCivil = Estados.GetEstadoCivil(GridLista.SelectedCells[5].Value.ToString()).IdEstadoCivil,
            };

            ParentWindow.LlenarDatos(LoadCliente);
            Dispose();
            Close();
        }
        #endregion

        #region Click BtnSalir
        private void BtnSalir_Click(Object sender, EventArgs e)
        {
            Dispose();
            Close();
        }
        #endregion
    }
}