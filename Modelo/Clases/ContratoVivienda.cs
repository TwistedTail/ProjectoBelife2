﻿using System;

namespace Modelo.Clases
{
    public class ContratoVivienda
    {
        public String Numero { get; set; }
        public Int32 CodigoPostal { get; set; }
    }
}