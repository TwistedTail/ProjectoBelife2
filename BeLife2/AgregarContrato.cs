﻿using Conexion;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Modelo.Clases;
using Modelo.Recursos;
using System;

namespace Vista
{
    public partial class AgregarContrato : MetroForm
    {
        private String RutActual { get; set; }
        private String DetalleActual { get; set; }
        private String TipoActual { get; set; }
        private String PlanActual { get; set; }
        private String NuevoContrato { get; set; }

        #region Constructor AgregarContrato()
        public AgregarContrato()
        {
            InitializeComponent();

            DAOTipoContrato Tipos = new DAOTipoContrato();
            Tipos.GetAllTipoContrato().ForEach(delegate (TipoContrato T) { ComboTipo.Items.Add(T.Descripcion); });

            LimpiarDatos();
        }
        #endregion

        #region GenerarNuevoContrato()
        private void GenerarNuevoContrato()
        {
            NuevoContrato = DateTime.Now.ToString("yyyyMMddHHmmss");
            TextNumero.Text = NuevoContrato;
        }
        #endregion

        #region EnableContratoExtra(Estado)
        private void EnableContratoExtra(Boolean Estado)
        {
            ComboBusqueda.Enabled = Estado;
            TextDetalle.Enabled = Estado;
            BtnBuscarDetalle.Enabled = Estado;
        }
        #endregion

        #region ActualizarClientes()
        public void ActualizarClientes()
        {
            ComboCliente.Items.Clear();
            ComboCliente.Items.Add("-N/A-");
            ComboCliente.SelectedIndex = 0;

            DAOCliente Clientes = new DAOCliente();

            Clientes.GetAllClientes().ForEach(delegate (Cliente C) { ComboCliente.Items.Add(C.RutCliente); });
        }
        #endregion

        #region ActualizarPlanes()
        public void ActualizarPlanes()
        {
            ComboPlan.Items.Clear();
            ComboPlan.Items.Add("-N/A-");
            ComboPlan.SelectedIndex = 0;

            DAOTipoContrato Tipos = new DAOTipoContrato();
            DAOPlan Planes = new DAOPlan();

            Int32 Tipo = Tipos.GetTipoContrato(TipoActual).IdTipoContrato;
            Planes.GetPlanesTipo(Tipo).ForEach(delegate (Plan P) { ComboPlan.Items.Add(P.Nombre); });

            ComboBusqueda.Items.Clear();
            ComboBusqueda.Items.Add("-N/A-");
            ComboBusqueda.SelectedIndex = 0;
            switch (TipoActual)
            {
                case "Vida":
                    LabelBusqueda.Text = "N/A";
                    LabelDetalle.Text = "N/A";

                    EnableContratoExtra(false);

                    if (ComboCliente.SelectedIndex <= 0) { ActualizarClientes(); }
                    break;
                case "Vehiculos":
                    LabelBusqueda.Text = "Patente del Vehiculo";
                    LabelDetalle.Text = "Modelo del Vehiculo";

                    EnableContratoExtra(true);

                    DAOVehiculo Vehiculos = new DAOVehiculo();
                    Vehiculos.GetAllVehiculos().ForEach(delegate (Vehiculo V) { ComboBusqueda.Items.Add(V.Patente); });
                    break;
                case "Hogar":
                    LabelBusqueda.Text = "Codigo Postal";
                    LabelDetalle.Text = "Direccion de la Vivienda";

                    EnableContratoExtra(true);

                    DAOVivienda Viviendas = new DAOVivienda();
                    Viviendas.GetAllViviendas().ForEach(delegate (Vivienda V) { ComboBusqueda.Items.Add(V.CodigoPostal); });
                    break;
            }
        }
        #endregion

        #region ActualizarFecha(FechaInicio, FechaTermino)
        private void ActualizarFecha(DateTime FechaInicio, DateTime FechaTermino)
        {
            TimePickerVigencia.MinDate = FechaInicio;
            TimePickerVigencia.MaxDate = FechaTermino;
            TimePickerVigencia.Value = FechaInicio;
            TimePickerFinVigencia.Value = FechaTermino;
            CheckVigente.Checked = FechaInicio <= DateTime.Today & DateTime.Today <= FechaTermino;
        }
        #endregion

        #region ActualizarDatos()
        private void ActualizarDatos()
        {
            switch (ComboTipo.Text)
            {
                case "Vehiculos":
                    if (ComboBusqueda.SelectedIndex == 0) { TextDetalle.Text = ""; return; }

                    Vehiculo Ve = new DAOVehiculo().GetVehiculo(DetalleActual);
                    String Marca = new DAOMarca().GetMarca(Ve.IdMarca).Descripcion;
                    String Modelo = new DAOModelo().GetModelo(Ve.IdModelo).Descripcion;

                    TextDetalle.Text = Marca + " " + Modelo + " " + Ve.Anno;
                    break;
                case "Hogar":
                    if (!Int32.TryParse(DetalleActual, out Int32 Codigo)) { TextDetalle.Text = "";  return; }

                    Vivienda Vi = new DAOVivienda().GetVivienda(Codigo);
                    String Comuna = new DAOComuna().GetComuna(Vi.IdComuna).NombreComuna;

                    TextDetalle.Text = Vi.Direccion + ", " + Comuna;
                    break;
            }

            if (ComboCliente.SelectedIndex == 0) { TextCliente.Text = ""; return; }

            Cliente Cli = new DAOCliente().GetCliente(RutActual);

            TextCliente.Text = Cli.Nombres + " " + Cli.Apellidos;
        }
        #endregion

        #region ActualizarPrima()
        private void ActualizarPrima()
        {
            Tarificador Tari = new Tarificador();
            Cliente Cli = new DAOCliente().GetCliente(RutActual);
            Double PrimaBase = new DAOPlan().GetPlan(PlanActual).PrimaBase;
            Double Recargo = 0;

            switch (ComboTipo.SelectedIndex)
            {
                case 0:
                    Recargo = Tari.GetRecargo(0, Cli, null);
                    break;
                case 1:
                    Vehiculo Veh = new DAOVehiculo().GetVehiculo(DetalleActual);

                    Recargo = Tari.GetRecargo(1, Cli, Veh);
                    break;
                case 2:
                    Int32.TryParse(DetalleActual, out Int32 Codigo);

                    Vivienda Viv = new DAOVivienda().GetVivienda(Convert.ToInt32(Codigo));

                    Recargo = Tari.GetRecargo(2, Viv, null);
                    break;
            }

            TextAnual.Text = Math.Round(PrimaBase + Recargo, 1).ToString();
            TextMensual.Text = PrimaBase.ToString();
        }
        #endregion

        #region CargarExtra(Extra)
        public void CargarExtra(String Extra)
        {
            ComboBusqueda.SelectedItem = Extra;
        }
        #endregion

        //Arreglar (No llena todos los datos si son Vehiculo o Vivienda)
        #region LlenarDatos(Contrato)
        public void LlenarDatos(Contrato C)
        {
            DAOPlan Planes = new DAOPlan();
            DAOContrato Contratos = new DAOContrato();

            Plan Pla = Planes.GetPlan(C.CodigoPlan);

            TextNumero.Text = C.Numero;
            ComboCliente.SelectedItem = C.RutCliente;
            ComboPlan.SelectedItem = Pla.Nombre;
            CheckVigente.Checked = Contratos.IsVigente(C.Numero);
            CheckSalud.Checked = C.DeclaracionSalud > 0;
            TextObservacion.Text = C.Observaciones;

            RutActual = C.RutCliente;

            ActualizarFecha(DateTime.Parse(C.FechaInicioVigencia), DateTime.Parse(C.FechaFinVigencia));
            ActualizarDatos();
            ActualizarPrima();
        }
        #endregion

        #region LimpiarDatos()
        private void LimpiarDatos()
        {
            GenerarNuevoContrato();

            ComboTipo.SelectedIndex = 0;
            ComboCliente.SelectedIndex = 0;
            TextObservacion.Text = "";

            ActualizarFecha(DateTime.Today, DateTime.Today.AddYears(1));
            ActualizarPlanes();
        }
        #endregion

        #region Click BtnBuscar
        private void BtnBuscar_Click(Object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TextNumero.Text) | String.IsNullOrWhiteSpace(TextNumero.Text)) { return; }

            DAOContrato Contratos = new DAOContrato();

            if (!Contratos.ExisteContrato(TextNumero.Text))
            {
                MetroMessageBox.Show(this, "Numero de Contrato Ingresado No Existe", "Error");
                return;
            }

            LlenarDatos(Contratos.GetContrato(TextNumero.Text));
        }
        #endregion

        #region Click BtnListar
        private void BtnListar_Click(Object sender, EventArgs e)
        {
            ListarContrato Ventana = new ListarContrato();
            StyleManager.Clone(Ventana);
            Ventana.MostrarBotonCargar(this);
            Ventana.ShowDialog();
        }
        #endregion

        #region Selection Changed ComboCliente
        private void ComboCliente_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Rut = (sender as MetroComboBox).Text;

            if (Rut == null) { return; }

            RutActual = Rut;
            ActualizarDatos();
        }
        #endregion

        #region Selection Changed ComboBusqueda
        private void ComboBusqueda_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Valor = (sender as MetroComboBox).Text;

            if (Valor == null) { return; }

            DetalleActual = Valor;
            ActualizarDatos();
            ActualizarPrima();
        }
        #endregion

        #region Click BtnBuscarDetalle
        private void BtnBuscarDetalle_Click(Object sender, EventArgs e)
        {
            AgregarExtraContrato Ventana = null;
            switch (TipoActual)
            {
                case "Vehiculos":
                    Ventana = new AgregarExtraContrato();
                    StyleManager.Clone(Ventana);
                    Ventana.HabilitarControlesVehiculo(this);
                    Ventana.ShowDialog();
                    Ventana.Dispose();
                    break;
                case "Hogar":
                    Ventana = new AgregarExtraContrato();
                    StyleManager.Clone(Ventana);
                    Ventana.HabilitarControlesVivienda(this);
                    Ventana.ShowDialog();
                    Ventana.Dispose();
                    break;
            }

        }
        #endregion

        #region Selection Changed ComboTipo
        private void ComboTipo_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Tipo = (sender as MetroComboBox).Text;

            if (Tipo == null) { return; }

            TipoActual = Tipo;
            DetalleActual = "N/A";
            ActualizarPlanes();
        }
        #endregion

        #region Selection Changed ComboPlan
        private void ComboPlan_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Nombre = (sender as MetroComboBox).Text;

            if (Nombre == null) { return; }

            PlanActual = new DAOPlan().GetPlanNombre(Nombre).IdPlan;
            ActualizarPrima();
        }
        #endregion

        #region Changed Value TimePickerVigencia
        private void TimePickerVigencia_ValueChanged(Object sender, EventArgs e)
        {
            DateTime Fecha = (sender as MetroDateTime).Value;

            if (Fecha == null) { return; }

            if (new DAOContrato().ExisteContrato(TextNumero.Text)) { return; }

            ActualizarFecha(Fecha, Fecha.AddYears(1));
        }
        #endregion

        #region Click BtnIngresar
        private void BtnIngresar_Click(Object sender, EventArgs e)
        {
            Boolean HasObservacion = !String.IsNullOrEmpty(TextObservacion.Text) & !String.IsNullOrWhiteSpace(TextObservacion.Text);
            Boolean HasRut = ComboCliente.SelectedIndex > 0;
            Boolean HasPlan = ComboPlan.SelectedIndex > 0;
            Boolean HasTipo = ComboTipo.SelectedIndex > 0;
            Boolean HasDetalle = ComboBusqueda.SelectedIndex > 0;

            if (!(HasRut & HasPlan) | HasTipo & !HasDetalle)
            {
                MetroMessageBox.Show(this, "Falta información por llenar en el formulario.", "Faltan Datos");
                return;
            }
            
            DAOContrato Contratos = new DAOContrato();

            String Numero = TextNumero.Text;
            String Creacion = DateTime.Today.ToShortDateString();
            String Termino = TimePickerFinVigencia.Value.ToShortDateString();
            String Rut = ComboCliente.Text;
            Int32 Tipo = (ComboTipo.SelectedIndex + 1) * 10;
            String Plan = new DAOPlan().GetPlanNombre(ComboPlan.Text).IdPlan;
            String Inicio = TimePickerVigencia.Value.ToShortDateString();
            String Fin = TimePickerFinVigencia.Value.ToShortDateString();
            Int32 Salud = CheckSalud.Checked ? 1 : 0;
            Double Anual = Double.Parse(TextAnual.Text);
            Double Mensual = Double.Parse(TextMensual.Text);
            String Observacion = HasObservacion ? TextObservacion.Text : "Sin Observacion.";

            String Mensaje = "";

            if (Contratos.ExisteContrato(Numero))
            {
                if (!Contratos.IsVigente(Numero))
                {
                    MetroMessageBox.Show(this, "No se pueden modificar contratos no vigentes.", "Contrato No Vigente");
                    return;
                }

                Contratos.ActualizarContrato(Numero, Creacion, Termino, Rut, Plan, Inicio, Fin, Salud, Anual, Mensual, Observacion);
                Mensaje = "Contrato actualizado exitosamente";
            }
            else
            {
                if (Contratos.ExisteTipoContrato(Rut, Plan) & Contratos.IsVigente(Rut, Plan))
                {
                    MetroMessageBox.Show(this, "Cliente ya posee un contrato vigente de este tipo.", "Contrato Existente");
                    return;
                }

                Contratos.AgregarContrato(NuevoContrato, Creacion, Termino, Rut, Tipo, Plan, Inicio, Fin, Salud, Anual, Mensual, Observacion);
                Mensaje = "Contrato ingresado exitosamente";
            }

            MetroMessageBox.Show(this, Mensaje, "Proceso Exitoso");
            LimpiarDatos();
        }
        #endregion

        //Arreglar, no cambia la wea (Revisar DAO?)
        #region Click BtnEliminar
        private void BtnEliminar_Click(Object sender, EventArgs e)
        {
            DAOContrato Contratos = new DAOContrato();

            Boolean HasNumero = !String.IsNullOrEmpty(TextNumero.Text) & !String.IsNullOrWhiteSpace(TextNumero.Text);
            Boolean HasRut = ComboCliente.SelectedIndex > 0;
            Boolean HasPlan = ComboPlan.SelectedIndex > 0;
            Boolean HasTipo = ComboTipo.SelectedIndex > 0;
            Boolean HasDetalle = ComboBusqueda.SelectedIndex > 0;

            if (!(HasNumero & HasRut & HasPlan) | HasTipo & !HasDetalle)
            {
                MetroMessageBox.Show(this, "Faltan datos por ingresar para poder finalizar este contrato.", "Faltan Datos");
                return;
            }
            else if (!Contratos.ExisteContrato(TextNumero.Text))
            {
                MetroMessageBox.Show(this, "Contrato ingresado no existe.", "Contrato Inexistente");
                return;
            }
            else if (!Contratos.IsVigente(TextNumero.Text))
            {
                MetroMessageBox.Show(this, "Contrato ingresado ya fue finalizado anteriormente.", "Contrato Ya Finalizado");
                return;
            }

            String Numero = TextNumero.Text;
            String Rut = ComboBusqueda.Text;
            String Plan = new DAOPlan().GetPlanNombre(ComboPlan.Text).IdPlan;

            Contratos.FinalizarContrato(Numero, Rut, Plan);
            MetroMessageBox.Show(this, "Contrato fue finalizado exitosamente.", "Proceso Exitoso");
        }
        #endregion

        #region Click BtnSalir
        private void BtnSalir_Click(Object sender, EventArgs e)
        {
            Dispose();
            Close();
        }
        #endregion
    }
}