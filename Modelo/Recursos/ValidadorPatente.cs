﻿using System;
using System.Text.RegularExpressions;

namespace Modelo.Recursos
{
    public class ValidadorPatente
    {
        private Regex Expresion1 = new Regex(@"^[A-Z]{2}[0-9]{4}$");
        private Regex Expresion2 = new Regex(@"^[A-Z]{3}[0-9]{3}$");
        private Regex Expresion3 = new Regex(@"^[A-Z]{4}[0-9]{2}$");

        #region ValidarPatente(Patente)
        public Boolean ValidarPatente(String Patente)
        {
            return Expresion1.IsMatch(Patente) | Expresion2.IsMatch(Patente) | Expresion3.IsMatch(Patente);
        }
        #endregion
    }
}