﻿using System;
using System.Text.RegularExpressions;

namespace Modelo.Recursos
{
    public class ValidadorRUT
    {
        private Regex PatronRut = new Regex("^([0-9]+-[0-9K])$");

        #region VerificarFormatoRut(Rut)
        private Boolean VerificarFormatoRut(String Rut)
        {
            return PatronRut.IsMatch(Rut);
        }
        #endregion

        #region ObtenerVerificador(Rut)
        private String ObtenerVerificador(Int32 Rut)
        {
            Int32 Suma = 0;
            Int32 Mult = 1;

            while (Rut != 0)
            {
                Mult++;
                if (Mult == 8) { Mult = 2; }
                Suma += (Rut % 10) * Mult;
                Rut = Rut / 10;
            }

            Suma = 11 - (Suma % 11);
            switch (Suma)
            {
                case 11: return "0";
                case 10: return "K";
                default: return Suma.ToString();
            }
        }
        #endregion

        #region ValidarRut(Rut)
        public Boolean ValidarRut(String Rut)
        {
            if (String.IsNullOrEmpty(Rut) | String.IsNullOrWhiteSpace(Rut)) { return false; }
            if (!VerificarFormatoRut(Rut)) { return false; }

            String[] rutCadena = Rut.Split('-');
            Int32 rutTemp = Int32.Parse(rutCadena[0].ToString());
            String verificadorTemp = ObtenerVerificador(rutTemp);

            if (rutCadena[1].ToString() != verificadorTemp) { return false; }

            return true;
        }
        #endregion
    }
}