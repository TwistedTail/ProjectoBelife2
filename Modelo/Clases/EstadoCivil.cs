﻿using System;

namespace Modelo.Clases
{
    public class EstadoCivil
    {
        public Int32 IdEstadoCivil { get; set; }
        public String Descripcion { get; set; }
    }
}