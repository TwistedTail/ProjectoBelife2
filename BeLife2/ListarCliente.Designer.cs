﻿namespace Vista
{
    partial class ListarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListarCliente));
            this.GridLista = new MetroFramework.Controls.MetroGrid();
            this.rut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nacimiente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estcivil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComboModo = new MetroFramework.Controls.MetroComboBox();
            this.ComboFiltro = new MetroFramework.Controls.MetroComboBox();
            this.BtnCargar = new MetroFramework.Controls.MetroButton();
            this.TextFiltro = new MetroFramework.Controls.MetroTextBox();
            this.BtnSalir = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.GridLista)).BeginInit();
            this.SuspendLayout();
            // 
            // GridLista
            // 
            this.GridLista.AccessibleName = "Lista de Resultados de Clientes";
            this.GridLista.AllowUserToAddRows = false;
            this.GridLista.AllowUserToDeleteRows = false;
            this.GridLista.AllowUserToResizeColumns = false;
            this.GridLista.AllowUserToResizeRows = false;
            this.GridLista.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GridLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GridLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GridLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.GridLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rut,
            this.nombres,
            this.apellidos,
            this.nacimiente,
            this.sexo,
            this.estcivil});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridLista.EnableHeadersVisualStyles = false;
            this.GridLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.GridLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GridLista.Location = new System.Drawing.Point(128, 216);
            this.GridLista.MultiSelect = false;
            this.GridLista.Name = "GridLista";
            this.GridLista.ReadOnly = true;
            this.GridLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridLista.RowHeadersWidth = 20;
            this.GridLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridLista.Size = new System.Drawing.Size(600, 304);
            this.GridLista.StandardTab = true;
            this.GridLista.TabIndex = 3;
            this.GridLista.UseStyleColors = true;
            // 
            // rut
            // 
            this.rut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.rut.HeaderText = "Rut Cliente";
            this.rut.Name = "rut";
            this.rut.ReadOnly = true;
            this.rut.Width = 80;
            // 
            // nombres
            // 
            this.nombres.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nombres.HeaderText = "Nombres";
            this.nombres.Name = "nombres";
            this.nombres.ReadOnly = true;
            this.nombres.Width = 76;
            // 
            // apellidos
            // 
            this.apellidos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.apellidos.HeaderText = "Apellidos";
            this.apellidos.Name = "apellidos";
            this.apellidos.ReadOnly = true;
            this.apellidos.Width = 78;
            // 
            // nacimiente
            // 
            this.nacimiente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nacimiente.HeaderText = "Fecha Nacimiento";
            this.nacimiente.Name = "nacimiente";
            this.nacimiente.ReadOnly = true;
            this.nacimiente.Width = 111;
            // 
            // sexo
            // 
            this.sexo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.sexo.HeaderText = "Sexo";
            this.sexo.Name = "sexo";
            this.sexo.ReadOnly = true;
            this.sexo.Width = 54;
            // 
            // estcivil
            // 
            this.estcivil.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.estcivil.HeaderText = "Estado Civil";
            this.estcivil.Name = "estcivil";
            this.estcivil.ReadOnly = true;
            this.estcivil.Width = 82;
            // 
            // ComboModo
            // 
            this.ComboModo.AccessibleName = "Menú Selección Modo de Busqueda";
            this.ComboModo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboModo.FormattingEnabled = true;
            this.ComboModo.ItemHeight = 23;
            this.ComboModo.Location = new System.Drawing.Point(128, 160);
            this.ComboModo.Name = "ComboModo";
            this.ComboModo.Size = new System.Drawing.Size(184, 29);
            this.ComboModo.TabIndex = 1;
            this.ComboModo.UseSelectable = true;
            this.ComboModo.SelectedIndexChanged += new System.EventHandler(this.ComboModo_SelectedIndexChanged);
            // 
            // ComboFiltro
            // 
            this.ComboFiltro.AccessibleName = "Menú Selección Filtro a Aplicar";
            this.ComboFiltro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboFiltro.FormattingEnabled = true;
            this.ComboFiltro.ItemHeight = 23;
            this.ComboFiltro.Location = new System.Drawing.Point(552, 160);
            this.ComboFiltro.Name = "ComboFiltro";
            this.ComboFiltro.Size = new System.Drawing.Size(176, 29);
            this.ComboFiltro.TabIndex = 2;
            this.ComboFiltro.UseSelectable = true;
            this.ComboFiltro.SelectedIndexChanged += new System.EventHandler(this.ComboFiltro_SelectedIndexChanged);
            // 
            // BtnCargar
            // 
            this.BtnCargar.AccessibleName = "Botón Cargar Cliente";
            this.BtnCargar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCargar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCargar.Location = new System.Drawing.Point(344, 160);
            this.BtnCargar.Name = "BtnCargar";
            this.BtnCargar.Size = new System.Drawing.Size(176, 29);
            this.BtnCargar.TabIndex = 4;
            this.BtnCargar.Text = "Cargar Cliente";
            this.BtnCargar.UseSelectable = true;
            this.BtnCargar.UseStyleColors = true;
            this.BtnCargar.Click += new System.EventHandler(this.BtnCargar_Click);
            // 
            // TextFiltro
            // 
            this.TextFiltro.AccessibleName = "Filtro del RUT de Cliente a Aplicar";
            this.TextFiltro.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextFiltro.CustomButton.Image = null;
            this.TextFiltro.CustomButton.Location = new System.Drawing.Point(148, 1);
            this.TextFiltro.CustomButton.Name = "";
            this.TextFiltro.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextFiltro.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextFiltro.CustomButton.TabIndex = 1;
            this.TextFiltro.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextFiltro.CustomButton.UseSelectable = true;
            this.TextFiltro.CustomButton.Visible = false;
            this.TextFiltro.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextFiltro.Lines = new string[0];
            this.TextFiltro.Location = new System.Drawing.Point(552, 160);
            this.TextFiltro.MaxLength = 10;
            this.TextFiltro.Name = "TextFiltro";
            this.TextFiltro.PasswordChar = '\0';
            this.TextFiltro.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextFiltro.SelectedText = "";
            this.TextFiltro.SelectionLength = 0;
            this.TextFiltro.SelectionStart = 0;
            this.TextFiltro.ShortcutsEnabled = true;
            this.TextFiltro.Size = new System.Drawing.Size(176, 29);
            this.TextFiltro.TabIndex = 2;
            this.TextFiltro.UseSelectable = true;
            this.TextFiltro.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextFiltro.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.TextFiltro.TextChanged += new System.EventHandler(this.TextFiltro_TextChanged);
            // 
            // BtnSalir
            // 
            this.BtnSalir.AccessibleName = "Botón volver";
            this.BtnSalir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSalir.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnSalir.Location = new System.Drawing.Point(128, 96);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(72, 32);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.Text = "Volver";
            this.BtnSalir.UseSelectable = true;
            this.BtnSalir.UseStyleColors = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(128, 136);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Modo de Busqueda";
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(552, 136);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(95, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Filtro a Aplicar";
            // 
            // ListarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.TextFiltro);
            this.Controls.Add(this.BtnCargar);
            this.Controls.Add(this.ComboFiltro);
            this.Controls.Add(this.ComboModo);
            this.Controls.Add(this.GridLista);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListarCliente";
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "Listado de Clientes";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            ((System.ComponentModel.ISupportInitialize)(this.GridLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid GridLista;
        private MetroFramework.Controls.MetroComboBox ComboModo;
        private MetroFramework.Controls.MetroTextBox TextFiltro;
        private MetroFramework.Controls.MetroButton BtnCargar;
        private MetroFramework.Controls.MetroComboBox ComboFiltro;
        private MetroFramework.Controls.MetroButton BtnSalir;
        private System.Windows.Forms.DataGridViewTextBoxColumn rut;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombres;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn nacimiente;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexo;
        private System.Windows.Forms.DataGridViewTextBoxColumn estcivil;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}