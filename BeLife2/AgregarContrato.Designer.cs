﻿namespace Vista
{
    partial class AgregarContrato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnSalir = new MetroFramework.Controls.MetroButton();
            this.BtnBuscar = new MetroFramework.Controls.MetroButton();
            this.BtnListar = new MetroFramework.Controls.MetroButton();
            this.BtnIngresar = new MetroFramework.Controls.MetroButton();
            this.BtnEliminar = new MetroFramework.Controls.MetroButton();
            this.TextNumero = new MetroFramework.Controls.MetroTextBox();
            this.ComboBusqueda = new MetroFramework.Controls.MetroComboBox();
            this.ComboPlan = new MetroFramework.Controls.MetroComboBox();
            this.TimePickerVigencia = new MetroFramework.Controls.MetroDateTime();
            this.CheckVigente = new MetroFramework.Controls.MetroCheckBox();
            this.TextDetalle = new MetroFramework.Controls.MetroTextBox();
            this.CheckSalud = new MetroFramework.Controls.MetroCheckBox();
            this.TextMensual = new MetroFramework.Controls.MetroTextBox();
            this.TextAnual = new MetroFramework.Controls.MetroTextBox();
            this.TextObservacion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.LabelBusqueda = new MetroFramework.Controls.MetroLabel();
            this.LabelDetalle = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.TimePickerFinVigencia = new MetroFramework.Controls.MetroDateTime();
            this.ComboTipo = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.BtnBuscarDetalle = new MetroFramework.Controls.MetroButton();
            this.BtnCliente = new MetroFramework.Controls.MetroButton();
            this.LabelCliente = new MetroFramework.Controls.MetroLabel();
            this.LabelComboCliente = new MetroFramework.Controls.MetroLabel();
            this.TextCliente = new MetroFramework.Controls.MetroTextBox();
            this.ComboCliente = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // BtnSalir
            // 
            this.BtnSalir.AccessibleName = "Botón Volver";
            this.BtnSalir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSalir.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnSalir.Location = new System.Drawing.Point(56, 112);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(56, 32);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.Text = "Volver";
            this.BtnSalir.UseSelectable = true;
            this.BtnSalir.UseStyleColors = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.AccessibleName = "Botón Buscar Numero de Contrato";
            this.BtnBuscar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnBuscar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnBuscar.Location = new System.Drawing.Point(336, 112);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(72, 32);
            this.BtnBuscar.TabIndex = 2;
            this.BtnBuscar.Text = "Buscar";
            this.BtnBuscar.UseSelectable = true;
            this.BtnBuscar.UseStyleColors = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // BtnListar
            // 
            this.BtnListar.AccessibleName = "Botón Abrir Listado de Contratos";
            this.BtnListar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnListar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnListar.Location = new System.Drawing.Point(440, 112);
            this.BtnListar.Name = "BtnListar";
            this.BtnListar.Size = new System.Drawing.Size(72, 32);
            this.BtnListar.TabIndex = 3;
            this.BtnListar.Text = "Listado";
            this.BtnListar.UseSelectable = true;
            this.BtnListar.UseStyleColors = true;
            this.BtnListar.Click += new System.EventHandler(this.BtnListar_Click);
            // 
            // BtnIngresar
            // 
            this.BtnIngresar.AccessibleName = "Botón Ingresar o Actualizar Contrato";
            this.BtnIngresar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnIngresar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnIngresar.Location = new System.Drawing.Point(280, 496);
            this.BtnIngresar.Name = "BtnIngresar";
            this.BtnIngresar.Size = new System.Drawing.Size(120, 56);
            this.BtnIngresar.TabIndex = 14;
            this.BtnIngresar.Text = "Ingresar Contrato";
            this.BtnIngresar.UseSelectable = true;
            this.BtnIngresar.UseStyleColors = true;
            this.BtnIngresar.Click += new System.EventHandler(this.BtnIngresar_Click);
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.AccessibleName = "Botón Finalizar Contrato";
            this.BtnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnEliminar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnEliminar.Location = new System.Drawing.Point(440, 496);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.Size = new System.Drawing.Size(120, 56);
            this.BtnEliminar.TabIndex = 15;
            this.BtnEliminar.Text = "Finalizar Contrato";
            this.BtnEliminar.UseSelectable = true;
            this.BtnEliminar.UseStyleColors = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // TextNumero
            // 
            this.TextNumero.AccessibleName = "Numero del Plan";
            this.TextNumero.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextNumero.CustomButton.Image = null;
            this.TextNumero.CustomButton.Location = new System.Drawing.Point(116, 1);
            this.TextNumero.CustomButton.Name = "";
            this.TextNumero.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextNumero.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextNumero.CustomButton.TabIndex = 1;
            this.TextNumero.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextNumero.CustomButton.UseSelectable = true;
            this.TextNumero.CustomButton.Visible = false;
            this.TextNumero.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextNumero.Lines = new string[0];
            this.TextNumero.Location = new System.Drawing.Point(152, 112);
            this.TextNumero.MaxLength = 14;
            this.TextNumero.Name = "TextNumero";
            this.TextNumero.PasswordChar = '\0';
            this.TextNumero.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextNumero.SelectedText = "";
            this.TextNumero.SelectionLength = 0;
            this.TextNumero.SelectionStart = 0;
            this.TextNumero.ShortcutsEnabled = true;
            this.TextNumero.Size = new System.Drawing.Size(144, 29);
            this.TextNumero.TabIndex = 1;
            this.TextNumero.UseSelectable = true;
            this.TextNumero.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextNumero.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // ComboBusqueda
            // 
            this.ComboBusqueda.AccessibleName = "Menú Selección RUT del Cliente";
            this.ComboBusqueda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboBusqueda.FormattingEnabled = true;
            this.ComboBusqueda.ItemHeight = 23;
            this.ComboBusqueda.Location = new System.Drawing.Point(152, 304);
            this.ComboBusqueda.Name = "ComboBusqueda";
            this.ComboBusqueda.Size = new System.Drawing.Size(144, 29);
            this.ComboBusqueda.TabIndex = 4;
            this.ComboBusqueda.UseSelectable = true;
            this.ComboBusqueda.SelectedIndexChanged += new System.EventHandler(this.ComboBusqueda_SelectedIndexChanged);
            // 
            // ComboPlan
            // 
            this.ComboPlan.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboPlan.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboPlan.FormattingEnabled = true;
            this.ComboPlan.ItemHeight = 23;
            this.ComboPlan.Location = new System.Drawing.Point(336, 240);
            this.ComboPlan.Name = "ComboPlan";
            this.ComboPlan.Size = new System.Drawing.Size(144, 29);
            this.ComboPlan.TabIndex = 7;
            this.ComboPlan.UseSelectable = true;
            this.ComboPlan.SelectedIndexChanged += new System.EventHandler(this.ComboPlan_SelectedIndexChanged);
            // 
            // TimePickerVigencia
            // 
            this.TimePickerVigencia.AccessibleName = "Selección de Fecha de Inicio de Vigencia";
            this.TimePickerVigencia.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TimePickerVigencia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TimePickerVigencia.Location = new System.Drawing.Point(152, 368);
            this.TimePickerVigencia.MinimumSize = new System.Drawing.Size(4, 29);
            this.TimePickerVigencia.Name = "TimePickerVigencia";
            this.TimePickerVigencia.Size = new System.Drawing.Size(144, 29);
            this.TimePickerVigencia.TabIndex = 910;
            this.TimePickerVigencia.ValueChanged += new System.EventHandler(this.TimePickerVigencia_ValueChanged);
            // 
            // CheckVigente
            // 
            this.CheckVigente.AccessibleName = "Checkbox Vigencia del Contrato";
            this.CheckVigente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckVigente.AutoSize = true;
            this.CheckVigente.Enabled = false;
            this.CheckVigente.Location = new System.Drawing.Point(504, 248);
            this.CheckVigente.Name = "CheckVigente";
            this.CheckVigente.Size = new System.Drawing.Size(113, 15);
            this.CheckVigente.TabIndex = 8;
            this.CheckVigente.Text = "Contrato Vigente";
            this.CheckVigente.UseSelectable = true;
            // 
            // TextDetalle
            // 
            this.TextDetalle.AccessibleName = "Nombre Completo del Cliente";
            this.TextDetalle.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextDetalle.CustomButton.Image = null;
            this.TextDetalle.CustomButton.Location = new System.Drawing.Point(292, 1);
            this.TextDetalle.CustomButton.Name = "";
            this.TextDetalle.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextDetalle.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextDetalle.CustomButton.TabIndex = 1;
            this.TextDetalle.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextDetalle.CustomButton.UseSelectable = true;
            this.TextDetalle.CustomButton.Visible = false;
            this.TextDetalle.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextDetalle.Lines = new string[0];
            this.TextDetalle.Location = new System.Drawing.Point(336, 304);
            this.TextDetalle.MaxLength = 41;
            this.TextDetalle.Name = "TextDetalle";
            this.TextDetalle.PasswordChar = '\0';
            this.TextDetalle.ReadOnly = true;
            this.TextDetalle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextDetalle.SelectedText = "";
            this.TextDetalle.SelectionLength = 0;
            this.TextDetalle.SelectionStart = 0;
            this.TextDetalle.ShortcutsEnabled = true;
            this.TextDetalle.Size = new System.Drawing.Size(320, 29);
            this.TextDetalle.TabIndex = 5;
            this.TextDetalle.UseSelectable = true;
            this.TextDetalle.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextDetalle.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // CheckSalud
            // 
            this.CheckSalud.AccessibleName = "Checkbox Declaracion de Salud";
            this.CheckSalud.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CheckSalud.AutoSize = true;
            this.CheckSalud.Location = new System.Drawing.Point(648, 248);
            this.CheckSalud.Name = "CheckSalud";
            this.CheckSalud.Size = new System.Drawing.Size(117, 15);
            this.CheckSalud.TabIndex = 9;
            this.CheckSalud.Text = "Declaracion Salud";
            this.CheckSalud.UseSelectable = true;
            // 
            // TextMensual
            // 
            this.TextMensual.AccessibleName = "Prima Mensual del Contrato";
            this.TextMensual.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextMensual.CustomButton.Image = null;
            this.TextMensual.CustomButton.Location = new System.Drawing.Point(116, 1);
            this.TextMensual.CustomButton.Name = "";
            this.TextMensual.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextMensual.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextMensual.CustomButton.TabIndex = 1;
            this.TextMensual.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextMensual.CustomButton.UseSelectable = true;
            this.TextMensual.CustomButton.Visible = false;
            this.TextMensual.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextMensual.Lines = new string[0];
            this.TextMensual.Location = new System.Drawing.Point(336, 432);
            this.TextMensual.MaxLength = 32767;
            this.TextMensual.Name = "TextMensual";
            this.TextMensual.PasswordChar = '\0';
            this.TextMensual.ReadOnly = true;
            this.TextMensual.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextMensual.SelectedText = "";
            this.TextMensual.SelectionLength = 0;
            this.TextMensual.SelectionStart = 0;
            this.TextMensual.ShortcutsEnabled = true;
            this.TextMensual.Size = new System.Drawing.Size(144, 29);
            this.TextMensual.TabIndex = 13;
            this.TextMensual.UseSelectable = true;
            this.TextMensual.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextMensual.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // TextAnual
            // 
            this.TextAnual.AccessibleName = "Prima Anual del Contrato";
            this.TextAnual.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextAnual.CustomButton.Image = null;
            this.TextAnual.CustomButton.Location = new System.Drawing.Point(116, 1);
            this.TextAnual.CustomButton.Name = "";
            this.TextAnual.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextAnual.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextAnual.CustomButton.TabIndex = 1;
            this.TextAnual.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextAnual.CustomButton.UseSelectable = true;
            this.TextAnual.CustomButton.Visible = false;
            this.TextAnual.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextAnual.Lines = new string[0];
            this.TextAnual.Location = new System.Drawing.Point(152, 432);
            this.TextAnual.MaxLength = 32767;
            this.TextAnual.Name = "TextAnual";
            this.TextAnual.PasswordChar = '\0';
            this.TextAnual.ReadOnly = true;
            this.TextAnual.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextAnual.SelectedText = "";
            this.TextAnual.SelectionLength = 0;
            this.TextAnual.SelectionStart = 0;
            this.TextAnual.ShortcutsEnabled = true;
            this.TextAnual.Size = new System.Drawing.Size(144, 29);
            this.TextAnual.TabIndex = 12;
            this.TextAnual.UseSelectable = true;
            this.TextAnual.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextAnual.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // TextObservacion
            // 
            this.TextObservacion.AccessibleName = "Observaciones del Contrato";
            this.TextObservacion.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextObservacion.CustomButton.Image = null;
            this.TextObservacion.CustomButton.Location = new System.Drawing.Point(162, 2);
            this.TextObservacion.CustomButton.Name = "";
            this.TextObservacion.CustomButton.Size = new System.Drawing.Size(91, 91);
            this.TextObservacion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextObservacion.CustomButton.TabIndex = 1;
            this.TextObservacion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextObservacion.CustomButton.UseSelectable = true;
            this.TextObservacion.CustomButton.Visible = false;
            this.TextObservacion.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextObservacion.Lines = new string[0];
            this.TextObservacion.Location = new System.Drawing.Point(512, 368);
            this.TextObservacion.MaxLength = 200;
            this.TextObservacion.Multiline = true;
            this.TextObservacion.Name = "TextObservacion";
            this.TextObservacion.PasswordChar = '\0';
            this.TextObservacion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextObservacion.SelectedText = "";
            this.TextObservacion.SelectionLength = 0;
            this.TextObservacion.SelectionStart = 0;
            this.TextObservacion.ShortcutsEnabled = true;
            this.TextObservacion.Size = new System.Drawing.Size(256, 96);
            this.TextObservacion.TabIndex = 14;
            this.TextObservacion.UseSelectable = true;
            this.TextObservacion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextObservacion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(152, 88);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(106, 19);
            this.metroLabel1.TabIndex = 16;
            this.metroLabel1.Text = "Numero de Plan";
            // 
            // LabelBusqueda
            // 
            this.LabelBusqueda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelBusqueda.AutoSize = true;
            this.LabelBusqueda.Location = new System.Drawing.Point(152, 280);
            this.LabelBusqueda.Name = "LabelBusqueda";
            this.LabelBusqueda.Size = new System.Drawing.Size(33, 19);
            this.LabelBusqueda.TabIndex = 17;
            this.LabelBusqueda.Text = "N/A";
            // 
            // LabelDetalle
            // 
            this.LabelDetalle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelDetalle.AutoSize = true;
            this.LabelDetalle.Location = new System.Drawing.Point(336, 280);
            this.LabelDetalle.Name = "LabelDetalle";
            this.LabelDetalle.Size = new System.Drawing.Size(33, 19);
            this.LabelDetalle.TabIndex = 18;
            this.LabelDetalle.Text = "N/A";
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(336, 216);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(105, 19);
            this.metroLabel4.TabIndex = 19;
            this.metroLabel4.Text = "Plan a Contratar";
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(152, 344);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(111, 19);
            this.metroLabel5.TabIndex = 20;
            this.metroLabel5.Text = "Inicio de Vigencia";
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(336, 344);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(128, 19);
            this.metroLabel6.TabIndex = 21;
            this.metroLabel6.Text = "Termino de Vigencia";
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(336, 408);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(96, 19);
            this.metroLabel7.TabIndex = 22;
            this.metroLabel7.Text = "Prima Mensual";
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(152, 408);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(81, 19);
            this.metroLabel8.TabIndex = 23;
            this.metroLabel8.Text = "Prima Anual";
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(512, 344);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(95, 19);
            this.metroLabel9.TabIndex = 24;
            this.metroLabel9.Text = "Observaciones";
            // 
            // TimePickerFinVigencia
            // 
            this.TimePickerFinVigencia.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TimePickerFinVigencia.Enabled = false;
            this.TimePickerFinVigencia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TimePickerFinVigencia.Location = new System.Drawing.Point(336, 368);
            this.TimePickerFinVigencia.MinimumSize = new System.Drawing.Size(4, 29);
            this.TimePickerFinVigencia.Name = "TimePickerFinVigencia";
            this.TimePickerFinVigencia.Size = new System.Drawing.Size(144, 29);
            this.TimePickerFinVigencia.TabIndex = 11;
            // 
            // ComboTipo
            // 
            this.ComboTipo.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboTipo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboTipo.FormattingEnabled = true;
            this.ComboTipo.ItemHeight = 23;
            this.ComboTipo.Location = new System.Drawing.Point(152, 240);
            this.ComboTipo.Name = "ComboTipo";
            this.ComboTipo.Size = new System.Drawing.Size(144, 29);
            this.ComboTipo.TabIndex = 6;
            this.ComboTipo.UseSelectable = true;
            this.ComboTipo.SelectedIndexChanged += new System.EventHandler(this.ComboTipo_SelectedIndexChanged);
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(152, 216);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(110, 19);
            this.metroLabel10.TabIndex = 26;
            this.metroLabel10.Text = "Tipo de Contrato";
            // 
            // BtnBuscarDetalle
            // 
            this.BtnBuscarDetalle.AccessibleName = "Botón Buscar Numero de Contrato";
            this.BtnBuscarDetalle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnBuscarDetalle.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnBuscarDetalle.Location = new System.Drawing.Point(696, 304);
            this.BtnBuscarDetalle.Name = "BtnBuscarDetalle";
            this.BtnBuscarDetalle.Size = new System.Drawing.Size(72, 32);
            this.BtnBuscarDetalle.TabIndex = 914;
            this.BtnBuscarDetalle.Text = "Buscar";
            this.BtnBuscarDetalle.UseSelectable = true;
            this.BtnBuscarDetalle.UseStyleColors = true;
            this.BtnBuscarDetalle.Click += new System.EventHandler(this.BtnBuscarDetalle_Click);
            // 
            // BtnCliente
            // 
            this.BtnCliente.AccessibleName = "Botón Buscar Numero de Contrato";
            this.BtnCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCliente.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCliente.Location = new System.Drawing.Point(696, 176);
            this.BtnCliente.Name = "BtnCliente";
            this.BtnCliente.Size = new System.Drawing.Size(72, 32);
            this.BtnCliente.TabIndex = 919;
            this.BtnCliente.Text = "Buscar";
            this.BtnCliente.UseSelectable = true;
            this.BtnCliente.UseStyleColors = true;
            // 
            // LabelCliente
            // 
            this.LabelCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelCliente.AutoSize = true;
            this.LabelCliente.Location = new System.Drawing.Point(336, 152);
            this.LabelCliente.Name = "LabelCliente";
            this.LabelCliente.Size = new System.Drawing.Size(125, 19);
            this.LabelCliente.TabIndex = 918;
            this.LabelCliente.Text = "Nombre del Cliente";
            // 
            // LabelComboCliente
            // 
            this.LabelComboCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelComboCliente.AutoSize = true;
            this.LabelComboCliente.Location = new System.Drawing.Point(152, 152);
            this.LabelComboCliente.Name = "LabelComboCliente";
            this.LabelComboCliente.Size = new System.Drawing.Size(99, 19);
            this.LabelComboCliente.TabIndex = 917;
            this.LabelComboCliente.Text = "RUT del Cliente";
            // 
            // TextCliente
            // 
            this.TextCliente.AccessibleName = "Nombre Completo del Cliente";
            this.TextCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextCliente.CustomButton.Image = null;
            this.TextCliente.CustomButton.Location = new System.Drawing.Point(292, 1);
            this.TextCliente.CustomButton.Name = "";
            this.TextCliente.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.TextCliente.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextCliente.CustomButton.TabIndex = 1;
            this.TextCliente.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextCliente.CustomButton.UseSelectable = true;
            this.TextCliente.CustomButton.Visible = false;
            this.TextCliente.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextCliente.Lines = new string[0];
            this.TextCliente.Location = new System.Drawing.Point(336, 176);
            this.TextCliente.MaxLength = 41;
            this.TextCliente.Name = "TextCliente";
            this.TextCliente.PasswordChar = '\0';
            this.TextCliente.ReadOnly = true;
            this.TextCliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextCliente.SelectedText = "";
            this.TextCliente.SelectionLength = 0;
            this.TextCliente.SelectionStart = 0;
            this.TextCliente.ShortcutsEnabled = true;
            this.TextCliente.Size = new System.Drawing.Size(320, 29);
            this.TextCliente.TabIndex = 916;
            this.TextCliente.UseSelectable = true;
            this.TextCliente.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextCliente.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // ComboCliente
            // 
            this.ComboCliente.AccessibleName = "Menú Selección RUT del Cliente";
            this.ComboCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboCliente.FormattingEnabled = true;
            this.ComboCliente.ItemHeight = 23;
            this.ComboCliente.Location = new System.Drawing.Point(152, 176);
            this.ComboCliente.Name = "ComboCliente";
            this.ComboCliente.Size = new System.Drawing.Size(144, 29);
            this.ComboCliente.TabIndex = 915;
            this.ComboCliente.UseSelectable = true;
            this.ComboCliente.SelectedIndexChanged += new System.EventHandler(this.ComboCliente_SelectedIndexChanged);
            // 
            // AgregarContrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.BtnCliente);
            this.Controls.Add(this.LabelCliente);
            this.Controls.Add(this.LabelComboCliente);
            this.Controls.Add(this.TextCliente);
            this.Controls.Add(this.ComboCliente);
            this.Controls.Add(this.BtnBuscarDetalle);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.ComboTipo);
            this.Controls.Add(this.TimePickerFinVigencia);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.LabelDetalle);
            this.Controls.Add(this.LabelBusqueda);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.TextObservacion);
            this.Controls.Add(this.TextAnual);
            this.Controls.Add(this.TextMensual);
            this.Controls.Add(this.CheckSalud);
            this.Controls.Add(this.TextDetalle);
            this.Controls.Add(this.CheckVigente);
            this.Controls.Add(this.TimePickerVigencia);
            this.Controls.Add(this.ComboPlan);
            this.Controls.Add(this.ComboBusqueda);
            this.Controls.Add(this.TextNumero);
            this.Controls.Add(this.BtnEliminar);
            this.Controls.Add(this.BtnIngresar);
            this.Controls.Add(this.BtnListar);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.BtnSalir);
            this.Name = "AgregarContrato";
            this.Text = "Ingreso de Contratos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton BtnSalir;
        private MetroFramework.Controls.MetroButton BtnBuscar;
        private MetroFramework.Controls.MetroButton BtnListar;
        private MetroFramework.Controls.MetroButton BtnIngresar;
        private MetroFramework.Controls.MetroButton BtnEliminar;
        private MetroFramework.Controls.MetroTextBox TextNumero;
        private MetroFramework.Controls.MetroComboBox ComboBusqueda;
        private MetroFramework.Controls.MetroComboBox ComboPlan;
        private MetroFramework.Controls.MetroDateTime TimePickerVigencia;
        private MetroFramework.Controls.MetroCheckBox CheckVigente;
        private MetroFramework.Controls.MetroTextBox TextDetalle;
        private MetroFramework.Controls.MetroTextBox TextObservacion;
        private MetroFramework.Controls.MetroTextBox TextAnual;
        private MetroFramework.Controls.MetroTextBox TextMensual;
        private MetroFramework.Controls.MetroCheckBox CheckSalud;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel LabelBusqueda;
        private MetroFramework.Controls.MetroLabel LabelDetalle;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroDateTime TimePickerFinVigencia;
        private MetroFramework.Controls.MetroComboBox ComboTipo;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroButton BtnBuscarDetalle;
        private MetroFramework.Controls.MetroButton BtnCliente;
        private MetroFramework.Controls.MetroLabel LabelCliente;
        private MetroFramework.Controls.MetroLabel LabelComboCliente;
        private MetroFramework.Controls.MetroTextBox TextCliente;
        private MetroFramework.Controls.MetroComboBox ComboCliente;
    }
}