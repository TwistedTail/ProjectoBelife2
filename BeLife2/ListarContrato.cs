﻿using Conexion;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Modelo.Clases;
using System;
using System.Collections.Generic;

namespace Vista
{
    public partial class ListarContrato : MetroForm
    {
        private String Numero { get; set; }
        private String Rut { get; set; }
        private String CodigoPlan { get; set; }
        private AgregarContrato ParentWindow { get; set; }

        #region Constructor ListarContrato()
        public ListarContrato()
        {
            InitializeComponent();

            BtnCargar.Enabled = false;
            BtnCargar.Visible = false;

            ComboModo.Items.Add("Filtrar por Numero de Contrato");
            ComboModo.Items.Add("Filtrar por Rut de Cliente");
            ComboModo.Items.Add("Filtrar por Nombre de Plan");
            ComboModo.SelectedIndex = 0;

            LlenarDatos();
        }
        #endregion

        #region MostrarBotonCargar(AgregarContrato)
        public void MostrarBotonCargar(AgregarContrato Ventana)
        {
            BtnCargar.Enabled = true;
            BtnCargar.Visible = true;

            ParentWindow = Ventana;
        }
        #endregion

        #region LlenarDatos()
        private void LlenarDatos()
        {
            GridLista.Rows.Clear();

            DAOTipoContrato Tipos = new DAOTipoContrato();
            DAOContrato Contratos = new DAOContrato();
            DAOPlan Planes = new DAOPlan();

            List<Contrato> Lista = Contratos.GetContratos(Numero,Rut,CodigoPlan);

            if (Lista.Count == 0) { return; }

            Lista.ForEach(delegate (Contrato C)
            {
                List<String> Fila = new List<String>
                {
                    C.Numero,
                    C.FechaCreacion,
                    C.FechaTermino,
                    C.RutCliente,
                    Tipos.GetTipoContrato(C.IdTipoContrato).Descripcion,
                    Planes.GetPlan(C.CodigoPlan).Nombre,
                    Contratos.IsVigente(C.Numero).ToString(),
                    C.FechaInicioVigencia,
                    C.FechaFinVigencia,
                    (C.DeclaracionSalud > 0).ToString(),
                    C.PrimaAnual.ToString(),
                    C.PrimaMensual.ToString(),
                    C.Observaciones
                };

                GridLista.Rows.Add(Fila.ToArray());
            });
        }
        #endregion

        //Revisar?
        #region Click BtnCargar
        private void BtnCargar_Click(Object sender, EventArgs e)
        {
            if (GridLista.SelectedCells.Count == 0) { return; }

            DAOTipoContrato Tipos = new DAOTipoContrato();
            DAOPlan Planes = new DAOPlan();

            Contrato LoadContrato = new Contrato
            {
                Numero = GridLista.SelectedCells[0].Value.ToString(),
                FechaCreacion = GridLista.SelectedCells[1].Value.ToString(),
                FechaTermino = GridLista.SelectedCells[2].Value.ToString(),
                RutCliente = GridLista.SelectedCells[3].Value.ToString(),
                IdTipoContrato = Tipos.GetTipoContrato(GridLista.SelectedCells[4].Value.ToString()).IdTipoContrato,
                CodigoPlan = Planes.GetPlanNombre(GridLista.SelectedCells[5].Value.ToString()).IdPlan,
                FechaInicioVigencia = GridLista.SelectedCells[7].Value.ToString(),
                FechaFinVigencia = GridLista.SelectedCells[8].Value.ToString(),
                DeclaracionSalud = Boolean.Parse(GridLista.SelectedCells[9].Value.ToString()) ? 1 : 0,
                PrimaAnual = Double.Parse(GridLista.SelectedCells[10].Value.ToString()),
                PrimaMensual = Double.Parse(GridLista.SelectedCells[11].Value.ToString()),
                Observaciones = GridLista.SelectedCells[12].Value.ToString()
            };

            ParentWindow.LlenarDatos(LoadContrato);
            Dispose();
            Close();
        }
        #endregion

        #region Selection Changed ComboModo
        private void ComboModo_SelectedIndexChanged(Object sender, EventArgs e)
        {
            Int32 Modo = (sender as MetroComboBox).SelectedIndex;

            ComboFiltro.Items.Clear();
            ComboFiltro.Items.Add("Sin Filtro");
            ComboFiltro.SelectedIndex = 0;

            switch (Modo)
            {
                case 0:
                    DAOContrato Contratos = new DAOContrato();
                    Contratos.GetAllContratos().ForEach(delegate (Contrato C) { ComboFiltro.Items.Add(C.Numero); });
                break;
                case 1:
                    DAOCliente Clientes = new DAOCliente();
                    Clientes.GetAllClientes().ForEach(delegate (Cliente C) { ComboFiltro.Items.Add(C.RutCliente); });
                break;
                case 2:
                    DAOPlan Planes = new DAOPlan();
                    Planes.GetAllPlanes().ForEach(delegate (Plan P) { ComboFiltro.Items.Add(P.Nombre); });
                break;
            }
        }
        #endregion

        #region Selection Changed ComboFiltro
        private void ComboFiltro_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Sel = (sender as MetroComboBox).Text;
            Int32 Modo = ComboModo.SelectedIndex;

            switch (Modo)
            {
                case 0:
                    Numero = Sel;
                    Rut = "";
                    CodigoPlan = "";
                break;
                case 1:
                    Numero = "";
                    Rut = Sel;
                    CodigoPlan = "";
                break;
                case 2:
                    Numero = "";
                    Rut = "";
                    CodigoPlan = new DAOPlan().GetPlanNombre(Sel).IdPlan;
                break;
            }

            LlenarDatos();
        }
        #endregion

        #region Click BtnSalir
        private void BtnSalir_Click(Object sender, EventArgs e)
        {
            Dispose();
            Close();
        }
        #endregion
    }
}
