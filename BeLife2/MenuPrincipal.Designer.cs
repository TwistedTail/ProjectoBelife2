﻿using MetroFramework;
using MetroFramework.Forms;

namespace Vista
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.BtnListarContrato = new MetroFramework.Controls.MetroButton();
            this.BtnIngresarContrato = new MetroFramework.Controls.MetroButton();
            this.ToggleModo = new MetroFramework.Controls.MetroToggle();
            this.BtnSalir = new MetroFramework.Controls.MetroButton();
            this.MetroStyleManager = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.BtnListarCliente = new MetroFramework.Controls.MetroButton();
            this.BtnIngresarCliente = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.MetroStyleManager)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnListarContrato
            // 
            this.BtnListarContrato.AccessibleName = "Botón Acceder al Listado de Contratos";
            this.BtnListarContrato.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnListarContrato.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnListarContrato.Location = new System.Drawing.Point(440, 360);
            this.BtnListarContrato.Name = "BtnListarContrato";
            this.BtnListarContrato.Size = new System.Drawing.Size(200, 151);
            this.BtnListarContrato.TabIndex = 3;
            this.BtnListarContrato.Text = "Listado de Contratos";
            this.BtnListarContrato.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnListarContrato.UseSelectable = true;
            this.BtnListarContrato.UseStyleColors = true;
            this.BtnListarContrato.Click += new System.EventHandler(this.BtnListarContrato_Click);
            // 
            // BtnIngresarContrato
            // 
            this.BtnIngresarContrato.AccessibleName = "Botón Acceder al Ingreso de Contratos";
            this.BtnIngresarContrato.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnIngresarContrato.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnIngresarContrato.Location = new System.Drawing.Point(200, 360);
            this.BtnIngresarContrato.Name = "BtnIngresarContrato";
            this.BtnIngresarContrato.Size = new System.Drawing.Size(200, 151);
            this.BtnIngresarContrato.TabIndex = 2;
            this.BtnIngresarContrato.Text = "Ingreso de Contratos";
            this.BtnIngresarContrato.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnIngresarContrato.UseSelectable = true;
            this.BtnIngresarContrato.UseStyleColors = true;
            this.BtnIngresarContrato.Click += new System.EventHandler(this.BtnIngresarContrato_Click);
            // 
            // ToggleModo
            // 
            this.ToggleModo.AccessibleName = "Switch Modo Oscuro";
            this.ToggleModo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ToggleModo.AutoSize = true;
            this.ToggleModo.Checked = true;
            this.ToggleModo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ToggleModo.Location = new System.Drawing.Point(560, 120);
            this.ToggleModo.Name = "ToggleModo";
            this.ToggleModo.Size = new System.Drawing.Size(80, 17);
            this.ToggleModo.TabIndex = 4;
            this.ToggleModo.Text = "On";
            this.ToggleModo.UseSelectable = true;
            this.ToggleModo.CheckedChanged += new System.EventHandler(this.ThemeToggle_CheckedChanged);
            // 
            // BtnSalir
            // 
            this.BtnSalir.AccessibleName = "Botón Salir de la Aplicación";
            this.BtnSalir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSalir.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnSalir.Location = new System.Drawing.Point(200, 112);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(60, 36);
            this.BtnSalir.TabIndex = 5;
            this.BtnSalir.Text = "Salir";
            this.BtnSalir.UseSelectable = true;
            this.BtnSalir.UseStyleColors = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // MetroStyleManager
            // 
            this.MetroStyleManager.Owner = this;
            this.MetroStyleManager.Style = MetroFramework.MetroColorStyle.Purple;
            this.MetroStyleManager.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(552, 96);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(91, 19);
            this.metroLabel1.TabIndex = 18;
            this.metroLabel1.Text = "Modo Oscuro";
            // 
            // BtnListarCliente
            // 
            this.BtnListarCliente.AccessibleName = "Botón Acceder al Listado de Clientes";
            this.BtnListarCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnListarCliente.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnListarCliente.Location = new System.Drawing.Point(440, 176);
            this.BtnListarCliente.Name = "BtnListarCliente";
            this.BtnListarCliente.Size = new System.Drawing.Size(200, 151);
            this.BtnListarCliente.TabIndex = 1;
            this.BtnListarCliente.Text = "Listado de Clientes";
            this.BtnListarCliente.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnListarCliente.UseSelectable = true;
            this.BtnListarCliente.UseStyleColors = true;
            this.BtnListarCliente.Click += new System.EventHandler(this.BtnListarCliente_Click);
            // 
            // BtnIngresarCliente
            // 
            this.BtnIngresarCliente.AccessibleName = "Botón Acceder al Ingreso de Clientes";
            this.BtnIngresarCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnIngresarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnIngresarCliente.CausesValidation = false;
            this.BtnIngresarCliente.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnIngresarCliente.Location = new System.Drawing.Point(200, 176);
            this.BtnIngresarCliente.Name = "BtnIngresarCliente";
            this.BtnIngresarCliente.Size = new System.Drawing.Size(200, 151);
            this.BtnIngresarCliente.TabIndex = 0;
            this.BtnIngresarCliente.Text = "Ingreso de Clientes";
            this.BtnIngresarCliente.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnIngresarCliente.UseSelectable = true;
            this.BtnIngresarCliente.UseStyleColors = true;
            this.BtnIngresarCliente.Click += new System.EventHandler(this.BtnIngresarCliente_Click);
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.BtnIngresarCliente);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.ToggleModo);
            this.Controls.Add(this.BtnListarContrato);
            this.Controls.Add(this.BtnIngresarContrato);
            this.Controls.Add(this.BtnListarCliente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuPrincipal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "Menú Principal";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            ((System.ComponentModel.ISupportInitialize)(this.MetroStyleManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton BtnListarContrato;
        private MetroFramework.Controls.MetroButton BtnIngresarContrato;
        private MetroFramework.Controls.MetroToggle ToggleModo;
        private MetroFramework.Controls.MetroButton BtnSalir;
        private MetroFramework.Components.MetroStyleManager MetroStyleManager;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton BtnListarCliente;
        private MetroFramework.Controls.MetroButton BtnIngresarCliente;
    }
}

