﻿using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;
using System;
using System.Windows.Forms;

namespace Vista
{
    public partial class MenuPrincipal : MetroForm
    {
        #region Constructor MenuPrincipal
        public MenuPrincipal()
        {
            InitializeComponent();

            StyleManager = MetroStyleManager;
        }
        #endregion

        #region Checked Changed ThemeToggle
        private void ThemeToggle_CheckedChanged(Object sender, EventArgs e)
        {
            Boolean Toggle = (sender as MetroToggle).Checked;

            StyleManager.Theme = !Toggle ? MetroThemeStyle.Light : MetroThemeStyle.Dark;
        }
        #endregion

        #region Click BtnIngresarCliente
        private void BtnIngresarCliente_Click(Object sender, EventArgs e)
        {
            AgregarCliente Ventana = new AgregarCliente();
            StyleManager.Clone(Ventana);
            Ventana.ShowDialog();
            Ventana.Dispose();
        }
        #endregion

        #region Click BtnListarCliente
        private void BtnListarCliente_Click(Object sender, EventArgs e)
        {
            ListarCliente Ventana = new ListarCliente();
            StyleManager.Clone(Ventana);
            Ventana.ShowDialog();
            Ventana.Dispose();
        }
        #endregion

        #region Click BtnIngresarContrato
        private void BtnIngresarContrato_Click(Object sender, EventArgs e)
        {
            AgregarContrato Ventana = new AgregarContrato();
            StyleManager.Clone(Ventana);
            Ventana.ShowDialog();
            Ventana.Dispose();
        }
        #endregion

        #region Click BtnListarContrato
        private void BtnListarContrato_Click(Object sender, EventArgs e)
        {
            ListarContrato Ventana = new ListarContrato();
            StyleManager.Clone(Ventana);
            Ventana.ShowDialog();
            Ventana.Dispose();
        }
        #endregion

        #region Click BtnSalir
        private void BtnSalir_Click(Object sender, EventArgs e) => Application.Exit();
        #endregion
    }
}