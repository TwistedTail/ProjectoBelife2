﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAORegion
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteRegion(IdRegion)
        public Boolean ExisteRegion(Int32 idRegion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(idRegion) from region where idRegion = :idRegion"
            };
            Comando.Parameters.Add(new OracleParameter("idRegion", idRegion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteRegion(Descripcion)
        public Boolean ExisteRegion(String nombreRegion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(idRegion) from region where nombreRegion = :nombreRegion"
            };
            Comando.Parameters.Add(new OracleParameter("nombreRegion", nombreRegion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetRegion(idRegion)
        public Region GetRegion(Int32 idRegion)
        {
            if (!ExisteRegion(idRegion)) { return new Region(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select nombreRegion from Region where idRegion = :idRegion"
            };
            Comando.Parameters.Add(new OracleParameter("idRegion", idRegion));
            OracleDataReader Reader = Comando.ExecuteReader();

            Region Result = null;
            while (Reader.Read())
            {
                Result = new Region
                {
                    IdRegion = idRegion,
                    NombreRegion = Reader["nombreRegion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetRegion(Descripcion)
        public Region GetRegion(String nombreRegion)
        {
            if (!ExisteRegion(nombreRegion)) { return new Region(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select idRegion from Region where nombreRegion = :nombreRegion"
            };
            Comando.Parameters.Add(new OracleParameter("nombreRegion", nombreRegion));
            OracleDataReader Reader = Comando.ExecuteReader();

            Region Result = null;
            while (Reader.Read())
            {
                Result = new Region
                {
                    IdRegion = Convert.ToInt32(Reader["idRegion"]),
                    NombreRegion = nombreRegion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllRegiones()
        public List<Region> GetAllRegiones()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select idRegion, nombreRegion from Region"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Region> Result = new List<Region>();
            while (Reader.Read())
            {
                Region r = new Region
                {
                    IdRegion = Convert.ToInt32(Reader["idRegion"]),
                    NombreRegion = Reader["nombreRegion"].ToString()
                };
                Result.Add(r);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}