﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOModelo
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteModelo(IdModelo)
        public Boolean ExisteModelo(Int32 IdModelo)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdModelo) from modeloVehiculo where IdModelo = :IdModelo"
            };
            Comando.Parameters.Add(new OracleParameter("IdModelo", IdModelo));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteModelo(IdModelo)
        public Boolean ExisteModelo(String Descripcion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdModelo) from modeloVehiculo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetModelo(IdModelo)
        public ModeloVehiculo GetModelo(Int32 IdModelo)
        {
            if (!ExisteModelo(IdModelo)) { return new ModeloVehiculo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Descripcion from modeloVehiculo where IdModelo = :IdModelo"
            };
            Comando.Parameters.Add(new OracleParameter("IdModelo", IdModelo));
            OracleDataReader Reader = Comando.ExecuteReader();

            ModeloVehiculo Result = null;
            while (Reader.Read())
            {
                Result = new ModeloVehiculo
                {
                    IdModelo = IdModelo,
                    Descripcion = Reader["Descripcion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetModelo(Descripcion)
        public ModeloVehiculo GetModelo(String Descripcion)
        {
            if (!ExisteModelo(Descripcion)) { return new ModeloVehiculo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdModelo from modeloVehiculo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            OracleDataReader Reader = Comando.ExecuteReader();

            ModeloVehiculo Result = null;
            while (Reader.Read())
            {
                Result = new ModeloVehiculo
                {
                    IdModelo = Convert.ToInt32(Reader["IdModelo"]),
                    Descripcion = Descripcion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllModelos()
        public List<ModeloVehiculo> GetAllModelos()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdModelo, Descripcion from modeloVehiculo"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<ModeloVehiculo> Result = new List<ModeloVehiculo>();
            while (Reader.Read())
            {
                ModeloVehiculo m = new ModeloVehiculo
                {
                    IdModelo = Convert.ToInt32(Reader["IdModelo"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(m);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetModelos(IdMarca)
        public List<ModeloVehiculo> GetModelos(Int32 IdMarca)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select mmv.IdModelo, Descripcion from marcaModeloVehiculo mmv join modeloVehiculo mv on mmv.IdModelo = mv.IdModelo where IdMarca = :IdMarca"
            };
            Comando.Parameters.Add(new OracleParameter("IdMarca", IdMarca));
            OracleDataReader Reader = Comando.ExecuteReader();

            List<ModeloVehiculo> Result = new List<ModeloVehiculo>();
            while (Reader.Read())
            {
                ModeloVehiculo m = new ModeloVehiculo
                {
                    IdModelo = Convert.ToInt32(Reader["IdModelo"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(m);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}
