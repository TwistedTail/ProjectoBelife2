﻿using System;

namespace Modelo.Clases
{
    public class ModeloVehiculo
    {
        public Int32 IdModelo { get; set; }
        public String Descripcion { get; set; }
    }
}
