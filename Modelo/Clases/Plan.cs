﻿using System;

namespace Modelo.Clases
{
    public class Plan
    {
        public String IdPlan { get; set; }
        public Int32 IdTipoContrato { get; set; }
        public String Nombre { get; set; }
        public Double PrimaBase { get; set; }
        public String PolizaActual { get; set; }
    }
}