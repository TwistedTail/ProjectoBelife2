﻿using System;

namespace Modelo.Clases
{
    public class Cliente
    {
        public String RutCliente { get; set; }
        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String FechaNacimiento { get; set; }
        public Int32 IdSexo { get; set; }
        public Int32 IdEstadoCivil { get; set; }
    }
}