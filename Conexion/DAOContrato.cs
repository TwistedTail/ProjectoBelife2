﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOContrato
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteContrato(Numero)
        public Boolean ExisteContrato(String Numero)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(Numero) from contrato where Numero = :Numero"
            };
            Comando.Parameters.Add(new OracleParameter("Numero", Numero));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteTipoContrato(RutCliente,CodigoPlan)
        public Boolean ExisteTipoContrato(String RutCliente, String CodigoPlan)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select numero, fechaFinVigencia from contrato where RutCliente = :RutCliente and CodigoPlan = :CodigoPlan"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("CodigoPlan", CodigoPlan));
            OracleDataReader Reader = Comando.ExecuteReader();

            String Numero = "";
            DateTime Fin = new DateTime();
            while (Reader.Read())
            {
                Numero = Reader["Numero"].ToString();
                Fin = Convert.ToDateTime(Reader["fechaFinVigencia"]).Date;
            }

            Comando.Dispose();
            Conexion.CloseConexion();

            return !Numero.Equals("") & Fin >= DateTime.Today.Date;
        }
        #endregion

        #region IsVigente(Numero)
        public Boolean IsVigente(String Numero)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select fechaFinVigencia from contrato where Numero = :Numero"
            };
            Comando.Parameters.Add(new OracleParameter("Numero", Numero));
            DateTime FechaTermino = DateTime.Parse(Comando.ExecuteScalar().ToString());

            Comando.Dispose();
            Conexion.CloseConexion();

            return FechaTermino.Date >= DateTime.Today.Date;
        }
        #endregion

        #region IsVigente(RutCliente, CodigoPlan)
        public Boolean IsVigente(String RutCliente, String CodigoPlan)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select fechaFinVigencia from contrato where rutCliente = :rutCliente and codigoPlan = :codigoPlan"
            };
            Comando.Parameters.Add(new OracleParameter("rutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("codigoPlan", CodigoPlan));
            OracleDataReader Reader = Comando.ExecuteReader();

            DateTime FechaTermino = DateTime.Today;
            while (Reader.Read())
            {
                FechaTermino = DateTime.Parse(Reader["fechaFinVigencia"].ToString());
            }

            Comando.Dispose();
            Conexion.CloseConexion();

            return FechaTermino.Date >= DateTime.Today.Date;
        }
        #endregion

        #region AgregarContrato(Numero,Creacion,Termino,RutCliente,TipoContrato,CodigoPlan,InicioVigencia,FinVigencia,DeclaracionSalud,PrimaAnual,PrimaMensual,Observaciones)
        public Boolean AgregarContrato(String Numero, String fechaCreacion, String fechaTermino, String RutCliente, Int32 TipoContrato, String CodigoPlan, String fechaInicioVigencia, String fechaFinVigencia, Int32 declaracionSalud, Double primaAnual, Double primaMensual, String observaciones)
        {
            if (ExisteContrato(Numero)) { return false; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "insert into contrato values (:Numero, :fechaCreacion, :fechaTermino, :RutCliente, :IdTipoContrato, :CodigoPlan, :fechaInicioVigencia, :fechaFinVigencia, :declaracionSalud, :primaAnual, :primaMensual, :observaciones)"
            };
            Comando.Parameters.Add(new OracleParameter("Numero", Numero));
            Comando.Parameters.Add(new OracleParameter("fechaCreacion", fechaCreacion));
            Comando.Parameters.Add(new OracleParameter("fechaTermino", fechaTermino));
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("IdTipoContrato", TipoContrato));
            Comando.Parameters.Add(new OracleParameter("CodigoPlan", CodigoPlan));
            Comando.Parameters.Add(new OracleParameter("fechaInicioVigencia", fechaInicioVigencia));
            Comando.Parameters.Add(new OracleParameter("fechaFinVigencia", fechaFinVigencia));
            Comando.Parameters.Add(new OracleParameter("declaracionSalud", declaracionSalud));
            Comando.Parameters.Add(new OracleParameter("primaAnual", primaAnual));
            Comando.Parameters.Add(new OracleParameter("primaMensual", primaMensual));
            Comando.Parameters.Add(new OracleParameter("observaciones", observaciones));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();

            return true;
        }
        #endregion

        #region ActualizarContrato(Numero,Creacion,Termino,RutCliente,CodigoPlan,InicioVigencia,FinVigencia,DeclaracionSalud,PrimaAnual,PrimaMensual,Observaciones)
        public Boolean ActualizarContrato(String Numero, String fechaCreacion, String fechaTermino, String RutCliente, String CodigoPlan, String fechaInicioVigencia, String fechaFinVigencia, Int32 declaracionSalud, Double primaAnual, Double primaMensual, String observaciones)
        {
            if (!ExisteContrato(Numero)) { return false; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "update contrato set fechaCreacion = :fechaCreacion, fechaTermino = :fechaTermino, RutCliente = :RutCliente, CodigoPlan = :CodigoPlan, fechaInicioVigencia = :fechaInicioVigencia, fechaFinVigencia = :fechaFinVigencia, declaracionSalud = :declaracionSalud, primaAnual = :primaAnual, primaMensual = :primaMensual, observaciones = :observaciones where Numero = :Numero"
            };
            Comando.Parameters.Add(new OracleParameter("fechaCreacion", fechaCreacion));
            Comando.Parameters.Add(new OracleParameter("fechaTermino", fechaTermino));
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("CodigoPlan", CodigoPlan));
            Comando.Parameters.Add(new OracleParameter("fechaInicioVigencia", fechaInicioVigencia));
            Comando.Parameters.Add(new OracleParameter("fechaFinVigencia", fechaFinVigencia));
            Comando.Parameters.Add(new OracleParameter("declaracionSalud", declaracionSalud));
            Comando.Parameters.Add(new OracleParameter("primaAnual", primaAnual));
            Comando.Parameters.Add(new OracleParameter("primaMensual", primaMensual));
            Comando.Parameters.Add(new OracleParameter("observaciones", observaciones));
            Comando.Parameters.Add(new OracleParameter("Numero", Numero));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();

            return true;
        }
        #endregion

        #region FinalizarContrato(Numero,RutCliente,CodigoPlan)
        public Boolean FinalizarContrato(String Numero, String RutCliente, String CodigoPlan)
        {
            if (!ExisteContrato(Numero) | !IsVigente(Numero)) { return false; }

            String fecha = DateTime.Today.Date.ToString();

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "update contrato set fechaTermino = :fechaTermino, fechaFinVigencia = :fechaFinVigencia where Numero = :Numero and RutCliente = :RutCliente and CodigoPlan = :CodigoPlan"
            };
            Comando.Parameters.Add(new OracleParameter("fechaTermino", fecha));
            Comando.Parameters.Add(new OracleParameter("fechaFinVigencia", fecha));
            Comando.Parameters.Add(new OracleParameter("Numero", Numero));
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("CodigoPlan", CodigoPlan));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();

            return true;
        }
        #endregion

        #region GetAllContratos()
        public List<Contrato> GetAllContratos()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Numero, fechaCreacion, fechaTermino, RutCliente, IdTipoContrato, CodigoPlan, fechaInicioVigencia, fechaFinVigencia, declaracionSalud, primaAnual, primaMensual, observaciones from contrato"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Contrato> Result = new List<Contrato>();
            while (Reader.Read())
            {
                Contrato c = new Contrato
                {
                    Numero = Reader["Numero"].ToString(),
                    FechaCreacion = Reader["fechaCreacion"].ToString(),
                    FechaTermino = Reader["fechaTermino"].ToString(),
                    RutCliente = Reader["RutCliente"].ToString(),
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    CodigoPlan = Reader["CodigoPlan"].ToString(),
                    FechaInicioVigencia = Reader["fechaInicioVigencia"].ToString(),
                    FechaFinVigencia = Reader["fechaFinVigencia"].ToString(),
                    DeclaracionSalud = Convert.ToInt32(Reader["declaracionSalud"]),
                    PrimaAnual = Convert.ToDouble(Reader["primaAnual"]),
                    PrimaMensual = Convert.ToDouble(Reader["primaMensual"]),
                    Observaciones = Reader["observaciones"].ToString()
                };
                Result.Add(c);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetContrato(Numero)
        public Contrato GetContrato(String NumeroContrato)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Numero, fechaCreacion, fechaTermino, RutCliente, CodigoPlan, fechaInicioVigencia, fechaFinVigencia, declaracionSalud, primaAnual, primaMensual, observaciones from contrato where Numero = :Numero"
            };
            Comando.Parameters.Add(new OracleParameter("Numero", NumeroContrato));
            OracleDataReader Reader = Comando.ExecuteReader();

            Contrato Result = new Contrato();
            while (Reader.Read())
            {
                Contrato c = new Contrato
                {
                    Numero = Reader["Numero"].ToString(),
                    FechaCreacion = Reader["fechaCreacion"].ToString(),
                    FechaTermino = Reader["fechaTermino"].ToString(),
                    RutCliente = Reader["RutCliente"].ToString(),
                    CodigoPlan = Reader["CodigoPlan"].ToString(),
                    FechaInicioVigencia = Reader["fechaInicioVigencia"].ToString(),
                    FechaFinVigencia = Reader["fechaFinVigencia"].ToString(),
                    DeclaracionSalud = Convert.ToInt32(Reader["declaracionSalud"]),
                    PrimaAnual = Convert.ToDouble(Reader["primaAnual"]),
                    PrimaMensual = Convert.ToDouble(Reader["primaMensual"]),
                    Observaciones = Reader["observaciones"].ToString()
                };
                Result = c;
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetContratos(Numero,RutCliente,CodigoPlan)
        public List<Contrato> GetContratos(String NumeroContrato, String RutCliente, String CodigoPlan)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion()
            };

            String CommandText = "select Numero, fechaCreacion, fechaTermino, RutCliente, IdTipoContrato, CodigoPlan, fechaInicioVigencia, fechaFinVigencia, declaracionSalud, primaAnual, primaMensual, observaciones from contrato";

            if (!NumeroContrato.Equals("Sin Filtro") & !String.IsNullOrWhiteSpace(NumeroContrato))
            {
                CommandText += " where Numero = :Numero";
                Comando.Parameters.Add(new OracleParameter("Numero", NumeroContrato));
            }
            else if (!RutCliente.Equals("Sin Filtro") & !String.IsNullOrWhiteSpace(RutCliente))
            {
                CommandText += " where RutCliente = :RutCliente";
                Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            }
            else if (!CodigoPlan.Equals("Sin Filtro") & !String.IsNullOrWhiteSpace(CodigoPlan))
            {
                CommandText += " where CodigoPlan = :CodigoPlan";
                Comando.Parameters.Add(new OracleParameter("CodigoPlan", CodigoPlan));
            }

            Comando.CommandText = CommandText;
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Contrato> Result = new List<Contrato>();
            while (Reader.Read())
            {
                Contrato c = new Contrato
                {
                    Numero = Reader["Numero"].ToString(),
                    FechaCreacion = Reader["fechaCreacion"].ToString(),
                    FechaTermino = Reader["fechaTermino"].ToString(),
                    RutCliente = Reader["RutCliente"].ToString(),
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    CodigoPlan = Reader["CodigoPlan"].ToString(),
                    FechaInicioVigencia = Reader["fechaInicioVigencia"].ToString(),
                    FechaFinVigencia = Reader["fechaFinVigencia"].ToString(),
                    DeclaracionSalud = Convert.ToInt32(Reader["declaracionSalud"]),
                    PrimaAnual = Convert.ToDouble(Reader["primaAnual"]),
                    PrimaMensual = Convert.ToDouble(Reader["primaMensual"]),
                    Observaciones = Reader["observaciones"].ToString()
                };
                Result.Add(c);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}