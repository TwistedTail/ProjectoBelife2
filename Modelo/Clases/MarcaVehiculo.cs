﻿using System;

namespace Modelo.Clases
{
    public class MarcaVehiculo
    {
        public Int32 IdMarca { get; set; }
        public String Descripcion { get; set; }
    }
}
