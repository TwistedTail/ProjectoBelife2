﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOVivienda
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteVivienda(CodigoPostal)
        public Boolean ExisteVivienda(Int32 CodigoPostal)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(CodigoPostal) from vivienda where CodigoPostal = :CodigoPostal"
            };
            Comando.Parameters.Add(new OracleParameter("CodigoPostal", CodigoPostal));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region AgregarVivienda(CodigoPostal, Anno, Direccion, ValorInmueble, ValorContenido, IdRegion, IdComuna)
        public void AgregarVivienda(Int32 CodigoPostal, Int32 Anno, String Direccion, Double ValorInmueble, Double ValorContenido, Int32 IdRegion, Int32 IdComuna)
        {
            if (ExisteVivienda(CodigoPostal)) { return; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "insert into vivienda values (:CodigoPostal, :Anno, :Direccion, :ValorInmueble, :ValorContenido, :IdRegion, :IdComuna)"
            };
            Comando.Parameters.Add(new OracleParameter("CodigoPostal", CodigoPostal));
            Comando.Parameters.Add(new OracleParameter("Anno", Anno));
            Comando.Parameters.Add(new OracleParameter("Direccion", Direccion));
            Comando.Parameters.Add(new OracleParameter("ValorInmueble", ValorInmueble));
            Comando.Parameters.Add(new OracleParameter("ValorContenido", ValorContenido));
            Comando.Parameters.Add(new OracleParameter("IdRegion", IdRegion));
            Comando.Parameters.Add(new OracleParameter("IdComuna", IdComuna));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion

        #region ActualizarVivienda(CodigoPostal, Anno, Direccion, ValorInmueble, ValorContenido, IdRegion, IdComuna)
        public void ActualizarVivienda(Int32 CodigoPostal, Int32 Anno, String Direccion, Double ValorInmueble, Double ValorContenido, Int32 IdRegion, Int32 IdComuna)
        {
            if (!ExisteVivienda(CodigoPostal)) { return; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "update set Anno = :Anno, Direccion = :Direccion, ValorInmueble = :ValorInmueble, ValorContenido = :ValorContenido, IdRegion = :IdRegion, IdComuna = :IdComuna where CodigoPostal = :CodigoPostal"
            };
            Comando.Parameters.Add(new OracleParameter("Anno", Anno));
            Comando.Parameters.Add(new OracleParameter("Direccion", Direccion));
            Comando.Parameters.Add(new OracleParameter("ValorInmueble", ValorInmueble));
            Comando.Parameters.Add(new OracleParameter("ValorContenido", ValorContenido));
            Comando.Parameters.Add(new OracleParameter("IdRegion", IdRegion));
            Comando.Parameters.Add(new OracleParameter("IdComuna", IdComuna));
            Comando.Parameters.Add(new OracleParameter("CodigoPostal", CodigoPostal));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion
        
        #region GetVivienda(CodigoPostal)
        public Vivienda GetVivienda(Int32 CodigoPostal)
        {
            if (!ExisteVivienda(CodigoPostal)) { return new Vivienda(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Anno, Direccion, ValorInmueble, ValorContenido, IdRegion, IdComuna from vivienda where CodigoPostal = :CodigoPostal"
            };
            Comando.Parameters.Add(new OracleParameter("CodigoPostal", CodigoPostal));
            OracleDataReader Reader = Comando.ExecuteReader();

            Vivienda Result = new Vivienda();
            while (Reader.Read())
            {
                Result = new Vivienda
                {
                    CodigoPostal = CodigoPostal,
                    Anno = Convert.ToInt32(Reader["Anno"]),
                    Direccion = Reader["Direccion"].ToString(),
                    ValorInmueble = Convert.ToDouble(Reader["ValorInmueble"]),
                    ValorContenido = Convert.ToDouble(Reader["ValorContenido"]),
                    IdRegion = Convert.ToInt32(Reader["IdRegion"]),
                    IdComuna = Convert.ToInt32(Reader["IdComuna"])
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllViviendas()
        public List<Vivienda> GetAllViviendas()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select CodigoPostal, Anno, Direccion, ValorInmueble, ValorContenido, IdRegion, IdComuna from vivienda"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Vivienda> Result = new List<Vivienda>();
            while (Reader.Read())
            {
                Vivienda v = new Vivienda
                {
                    CodigoPostal = Convert.ToInt32(Reader["CodigoPostal"]),
                    Anno = Convert.ToInt32(Reader["Anno"]),
                    Direccion = Reader["Direccion"].ToString(),
                    ValorInmueble = Convert.ToDouble(Reader["ValorInmueble"]),
                    ValorContenido = Convert.ToDouble(Reader["ValorContenido"]),
                    IdRegion = Convert.ToInt32(Reader["IdRegion"]),
                    IdComuna = Convert.ToInt32(Reader["IdComuna"])
                };
                Result.Add(v);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}