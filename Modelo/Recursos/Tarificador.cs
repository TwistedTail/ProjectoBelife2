﻿using Modelo.Clases;
using System;

namespace Modelo.Recursos
{
    public class Tarificador
    {
        #region GetEdadCliente(Cliente)
        private Int32 GetEdadCliente(Cliente C)
        {
            if (C.FechaNacimiento == null) { return 0; }

            DateTime Today = DateTime.Today;
            DateTime FecNac = DateTime.Parse(C.FechaNacimiento);
            Int32 Edad = Today.Year - FecNac.Year; if (FecNac > Today.AddYears(-Edad)) { Edad--; }

            return Edad;
        }
        #endregion

        #region GetRecargo(Opcion, Clase1, Clase2)
        public Double GetRecargo(Int32 Opcion, Object Clase1, Object Clase2)
        {
            switch (Opcion)
            {
                case 0:
                    return GetRecargoCliente(Clase1 as Cliente);
                case 1:
                    return GetRecargoVehiculo(Clase1 as Cliente, Clase2 as Vehiculo);
                case 2:
                    return GetRecargoVivienda(Clase1 as Vivienda);
            }
            return 0;
        }
        #endregion

        #region GetRecargoCliente(Cliente)
        private Double GetRecargoCliente(Cliente C)
        {
            Int32 Edad = GetEdadCliente(C);
            Double Prima = 0;

            if (Edad > 45) { Prima += 6; }
            else if (Edad <= 45 & Edad >= 26) { Prima += 2.4; }
            else if (Edad <= 25 & Edad >= 18) { Prima += 3.6; }

            if (C.IdSexo == 1) { Prima += 2.4; }
            else if (C.IdSexo == 2) { Prima += 1.2; }

            if (C.IdEstadoCivil == 1) { Prima += 4.8; }
            else if (C.IdEstadoCivil == 2) { Prima += 2.4; }
            else if (C.IdEstadoCivil >= 3) { Prima += 3.6; }

            return Prima;
        }
        #endregion

        #region GetRecargoVehiculo(Cliente, Vehiculo)
        private Double GetRecargoVehiculo(Cliente C, Vehiculo V)
        {
            Int32 Annos = DateTime.Today.Year - V.Anno;
            Int32 Edad = GetEdadCliente(C);
            Double Prima = 0;

            if (Edad > 45) { Prima += 3.2; }
            else if (Edad <= 45 & Edad >= 26) { Prima += 2.4; }
            else if (Edad <= 25 & Edad >= 18) { Prima += 1.2; }

            if (C.IdSexo == 1) { Prima += 0.8; }
            else if (C.IdSexo == 2) { Prima += 0.4; }

            if (Annos == 0) { Prima += 1.2; }
            else if (Annos > 0 & Annos <= 5) { Prima += 0.8; }
            else if (Annos > 5) { Prima += 0.4; }

            return Prima;
        }
        #endregion

        #region GetRecargoVivienda(Vivienda)
        private Double GetRecargoVivienda(Vivienda V)
        {
            Int32 Annos = DateTime.Today.Year - V.Anno;
            Double Prima = 0;

            if (Annos >= 0 & Annos <= 5) { Prima += 1; }
            else if (Annos <= 10) { Prima += 0.8; }
            else if (Annos <= 30) { Prima += 0.4; }
            else { Prima += 0.2; }

            if (V.IdRegion == 13) { Prima += 3.2; }
            else { Prima += 2.8; }

            return Prima + V.ValorInmueble * 0.0003 + V.ValorContenido * 0.0007;
        }
        #endregion
    }
}