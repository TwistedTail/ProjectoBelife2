﻿using Oracle.DataAccess.Client;
using Modelo.Clases;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOTipoContrato
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteTipoContrato(IdTipoContrato)
        public Boolean ExisteTipoContrato(Int32 IdTipoContrato)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdTipoContrato) from tipoContrato where IdTipoContrato = :IdTipoContrato"
            };
            Comando.Parameters.Add(new OracleParameter("IdTipoContrato", IdTipoContrato));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteTipoContrato(Descripcion)
        public Boolean ExisteTipoContrato(String Descripcion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdTipoContrato) from tipoContrato where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetAllTipoContrato()
        public List<TipoContrato> GetAllTipoContrato()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdTipoContrato, Descripcion from tipoContrato"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<TipoContrato> Result = new List<TipoContrato>();
            while (Reader.Read())
            {
                TipoContrato tc = new TipoContrato
                {
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(tc);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetTipoContrato(IdTipoContrato)
        public TipoContrato GetTipoContrato(Int32 IdTipoContrato)
        {
            if (!ExisteTipoContrato(IdTipoContrato)) { return new TipoContrato(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Descripcion from tipoContrato where IdTipoContrato = :IdTipoContrato"
            };
            Comando.Parameters.Add(new OracleParameter("IdTipoContrato", IdTipoContrato));
            OracleDataReader Reader = Comando.ExecuteReader();

            TipoContrato Result = null;
            while (Reader.Read())
            {
                Result = new TipoContrato
                {
                    IdTipoContrato = IdTipoContrato,
                    Descripcion = Reader["Descripcion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetTipoContrato(Descripcion)
        public TipoContrato GetTipoContrato(String Descripcion)
        {
            if (!ExisteTipoContrato(Descripcion)) { return new TipoContrato(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdTipoContrato from tipoContrato where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            OracleDataReader Reader = Comando.ExecuteReader();

            TipoContrato Result = null;
            while (Reader.Read())
            {
                Result = new TipoContrato
                {
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    Descripcion = Descripcion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}