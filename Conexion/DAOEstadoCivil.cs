﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOEstadoCivil
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteEstadoCivil(IdEstadoCivil)
        public Boolean ExisteEstadoCivil(Int32 IdEstadoCivil)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdEstadoCivil) from estadoCivil where IdEstadoCivil = :IdEstadoCivil"
            };
            Comando.Parameters.Add(new OracleParameter("IdEstadoCivil", IdEstadoCivil));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteEstadoCivil(Descripcion)
        public Boolean ExisteEstadoCivil(String Descripcion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdEstadoCivil) from estadoCivil where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetAllEstadoCivil()
        public List<EstadoCivil> GetAllEstadoCivil()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdEstadoCivil, Descripcion from estadoCivil"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<EstadoCivil> Result = new List<EstadoCivil>();
            while (Reader.Read())
            {
                EstadoCivil ec = new EstadoCivil
                {
                    IdEstadoCivil = Convert.ToInt32(Reader["IdEstadoCivil"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(ec);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetEstadoCivil(IdEstadoCivil)
        public EstadoCivil GetEstadoCivil(Int32 IdEstadoCivil)
        {
            if (!ExisteEstadoCivil(IdEstadoCivil)) { return new EstadoCivil(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Descripcion from estadoCivil where IdEstadoCivil = :IdEstadoCivil"
            };
            Comando.Parameters.Add(new OracleParameter("IdEstadoCivil", IdEstadoCivil));
            OracleDataReader Reader = Comando.ExecuteReader();

            EstadoCivil Result = null;
            while (Reader.Read())
            {
                Result = new EstadoCivil
                {
                    IdEstadoCivil = IdEstadoCivil,
                    Descripcion = Reader["Descripcion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetEstadoCivil(Descripcion)
        public EstadoCivil GetEstadoCivil(String Descripcion)
        {
            if (!ExisteEstadoCivil(Descripcion)) { return new EstadoCivil(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdEstadoCivil from estadoCivil where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            OracleDataReader Reader = Comando.ExecuteReader();

            EstadoCivil Result = null;
            while (Reader.Read())
            {
                Result = new EstadoCivil
                {
                    IdEstadoCivil = Convert.ToInt32(Reader["IdEstadoCivil"]),
                    Descripcion = Descripcion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}