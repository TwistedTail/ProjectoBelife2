﻿namespace Vista
{
    partial class AgregarExtraContrato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextPatente = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.TextCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.ComboMarca = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.ComboModelo = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.ComboAnnoVehiculo = new MetroFramework.Controls.MetroComboBox();
            this.BtnBuscarVehiculo = new MetroFramework.Controls.MetroButton();
            this.BtnAgregarVehiculo = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.ComboAnnoVivienda = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.TextDireccion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.TextInmueble = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.TextContenido = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.ComboRegion = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.ComboComuna = new MetroFramework.Controls.MetroComboBox();
            this.BtnBuscarVivienda = new MetroFramework.Controls.MetroButton();
            this.BtnAgregarVivienda = new MetroFramework.Controls.MetroButton();
            this.BtnCancelar1 = new MetroFramework.Controls.MetroButton();
            this.BtnCargarVehiculo = new MetroFramework.Controls.MetroButton();
            this.BtnCargarVivienda = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // TextPatente
            // 
            this.TextPatente.AccessibleName = "Numero del Plan";
            this.TextPatente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextPatente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextPatente.CustomButton.Image = null;
            this.TextPatente.CustomButton.Location = new System.Drawing.Point(87, 1);
            this.TextPatente.CustomButton.Name = "";
            this.TextPatente.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextPatente.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextPatente.CustomButton.TabIndex = 1;
            this.TextPatente.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextPatente.CustomButton.UseSelectable = true;
            this.TextPatente.CustomButton.Visible = false;
            this.TextPatente.Enabled = false;
            this.TextPatente.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextPatente.Lines = new string[0];
            this.TextPatente.Location = new System.Drawing.Point(160, 104);
            this.TextPatente.MaxLength = 6;
            this.TextPatente.Name = "TextPatente";
            this.TextPatente.PasswordChar = '\0';
            this.TextPatente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextPatente.SelectedText = "";
            this.TextPatente.SelectionLength = 0;
            this.TextPatente.SelectionStart = 0;
            this.TextPatente.ShortcutsEnabled = true;
            this.TextPatente.Size = new System.Drawing.Size(144, 29);
            this.TextPatente.TabIndex = 1;
            this.TextPatente.UseSelectable = true;
            this.TextPatente.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextPatente.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(160, 80);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(52, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Patente";
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(160, 296);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(91, 19);
            this.metroLabel2.TabIndex = 19;
            this.metroLabel2.Text = "Codigo Postal";
            // 
            // TextCodigo
            // 
            this.TextCodigo.AccessibleName = "Numero del Plan";
            this.TextCodigo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextCodigo.CustomButton.Image = null;
            this.TextCodigo.CustomButton.Location = new System.Drawing.Point(87, 1);
            this.TextCodigo.CustomButton.Name = "";
            this.TextCodigo.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextCodigo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextCodigo.CustomButton.TabIndex = 1;
            this.TextCodigo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextCodigo.CustomButton.UseSelectable = true;
            this.TextCodigo.CustomButton.Visible = false;
            this.TextCodigo.Enabled = false;
            this.TextCodigo.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextCodigo.Lines = new string[0];
            this.TextCodigo.Location = new System.Drawing.Point(160, 320);
            this.TextCodigo.MaxLength = 7;
            this.TextCodigo.Name = "TextCodigo";
            this.TextCodigo.PasswordChar = '\0';
            this.TextCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextCodigo.SelectedText = "";
            this.TextCodigo.SelectionLength = 0;
            this.TextCodigo.SelectionStart = 0;
            this.TextCodigo.ShortcutsEnabled = true;
            this.TextCodigo.Size = new System.Drawing.Size(144, 29);
            this.TextCodigo.TabIndex = 1;
            this.TextCodigo.UseSelectable = true;
            this.TextCodigo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextCodigo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(160, 144);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(46, 19);
            this.metroLabel10.TabIndex = 28;
            this.metroLabel10.Text = "Marca";
            // 
            // ComboMarca
            // 
            this.ComboMarca.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboMarca.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboMarca.Enabled = false;
            this.ComboMarca.FormattingEnabled = true;
            this.ComboMarca.ItemHeight = 23;
            this.ComboMarca.Location = new System.Drawing.Point(160, 168);
            this.ComboMarca.Name = "ComboMarca";
            this.ComboMarca.Size = new System.Drawing.Size(144, 29);
            this.ComboMarca.TabIndex = 3;
            this.ComboMarca.UseSelectable = true;
            this.ComboMarca.SelectedIndexChanged += new System.EventHandler(this.ComboMarca_SelectedIndexChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(340, 145);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(55, 19);
            this.metroLabel3.TabIndex = 30;
            this.metroLabel3.Text = "Modelo";
            // 
            // ComboModelo
            // 
            this.ComboModelo.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboModelo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboModelo.Enabled = false;
            this.ComboModelo.FormattingEnabled = true;
            this.ComboModelo.ItemHeight = 23;
            this.ComboModelo.Location = new System.Drawing.Point(344, 168);
            this.ComboModelo.Name = "ComboModelo";
            this.ComboModelo.Size = new System.Drawing.Size(144, 29);
            this.ComboModelo.TabIndex = 4;
            this.ComboModelo.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(528, 144);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(122, 19);
            this.metroLabel4.TabIndex = 32;
            this.metroLabel4.Text = "Año de Fabricacion";
            // 
            // ComboAnnoVehiculo
            // 
            this.ComboAnnoVehiculo.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboAnnoVehiculo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboAnnoVehiculo.Enabled = false;
            this.ComboAnnoVehiculo.FormattingEnabled = true;
            this.ComboAnnoVehiculo.ItemHeight = 23;
            this.ComboAnnoVehiculo.Location = new System.Drawing.Point(528, 168);
            this.ComboAnnoVehiculo.Name = "ComboAnnoVehiculo";
            this.ComboAnnoVehiculo.Size = new System.Drawing.Size(144, 29);
            this.ComboAnnoVehiculo.TabIndex = 5;
            this.ComboAnnoVehiculo.UseSelectable = true;
            // 
            // BtnBuscarVehiculo
            // 
            this.BtnBuscarVehiculo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnBuscarVehiculo.Enabled = false;
            this.BtnBuscarVehiculo.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnBuscarVehiculo.Location = new System.Drawing.Point(344, 104);
            this.BtnBuscarVehiculo.Name = "BtnBuscarVehiculo";
            this.BtnBuscarVehiculo.Size = new System.Drawing.Size(72, 32);
            this.BtnBuscarVehiculo.TabIndex = 2;
            this.BtnBuscarVehiculo.Text = "Buscar";
            this.BtnBuscarVehiculo.UseSelectable = true;
            this.BtnBuscarVehiculo.UseStyleColors = true;
            this.BtnBuscarVehiculo.Click += new System.EventHandler(this.BtnBuscarVehiculo_Click);
            // 
            // BtnAgregarVehiculo
            // 
            this.BtnAgregarVehiculo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnAgregarVehiculo.Enabled = false;
            this.BtnAgregarVehiculo.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnAgregarVehiculo.Location = new System.Drawing.Point(344, 232);
            this.BtnAgregarVehiculo.Name = "BtnAgregarVehiculo";
            this.BtnAgregarVehiculo.Size = new System.Drawing.Size(144, 32);
            this.BtnAgregarVehiculo.TabIndex = 6;
            this.BtnAgregarVehiculo.Text = "Agregar";
            this.BtnAgregarVehiculo.UseSelectable = true;
            this.BtnAgregarVehiculo.UseStyleColors = true;
            this.BtnAgregarVehiculo.Click += new System.EventHandler(this.BtnAgregarVehiculo_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(160, 360);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(131, 19);
            this.metroLabel5.TabIndex = 3;
            this.metroLabel5.Text = "Año de Construccion";
            // 
            // ComboAnnoVivienda
            // 
            this.ComboAnnoVivienda.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboAnnoVivienda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboAnnoVivienda.Enabled = false;
            this.ComboAnnoVivienda.FormattingEnabled = true;
            this.ComboAnnoVivienda.ItemHeight = 23;
            this.ComboAnnoVivienda.Location = new System.Drawing.Point(160, 384);
            this.ComboAnnoVivienda.Name = "ComboAnnoVivienda";
            this.ComboAnnoVivienda.Size = new System.Drawing.Size(144, 29);
            this.ComboAnnoVivienda.TabIndex = 3;
            this.ComboAnnoVivienda.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(344, 360);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(63, 19);
            this.metroLabel6.TabIndex = 35;
            this.metroLabel6.Text = "Direccion";
            // 
            // TextDireccion
            // 
            this.TextDireccion.AccessibleName = "Numero del Plan";
            this.TextDireccion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextDireccion.CustomButton.Image = null;
            this.TextDireccion.CustomButton.Location = new System.Drawing.Point(225, 1);
            this.TextDireccion.CustomButton.Name = "";
            this.TextDireccion.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextDireccion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextDireccion.CustomButton.TabIndex = 1;
            this.TextDireccion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextDireccion.CustomButton.UseSelectable = true;
            this.TextDireccion.CustomButton.Visible = false;
            this.TextDireccion.Enabled = false;
            this.TextDireccion.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextDireccion.Lines = new string[0];
            this.TextDireccion.Location = new System.Drawing.Point(344, 384);
            this.TextDireccion.MaxLength = 40;
            this.TextDireccion.Name = "TextDireccion";
            this.TextDireccion.PasswordChar = '\0';
            this.TextDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextDireccion.SelectedText = "";
            this.TextDireccion.SelectionLength = 0;
            this.TextDireccion.SelectionStart = 0;
            this.TextDireccion.ShortcutsEnabled = true;
            this.TextDireccion.Size = new System.Drawing.Size(328, 29);
            this.TextDireccion.TabIndex = 4;
            this.TextDireccion.UseSelectable = true;
            this.TextDireccion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextDireccion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(160, 424);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(124, 19);
            this.metroLabel7.TabIndex = 37;
            this.metroLabel7.Text = "Valor Inmueble (UF)";
            // 
            // TextInmueble
            // 
            this.TextInmueble.AccessibleName = "Numero del Plan";
            this.TextInmueble.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextInmueble.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextInmueble.CustomButton.Image = null;
            this.TextInmueble.CustomButton.Location = new System.Drawing.Point(87, 1);
            this.TextInmueble.CustomButton.Name = "";
            this.TextInmueble.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextInmueble.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextInmueble.CustomButton.TabIndex = 1;
            this.TextInmueble.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextInmueble.CustomButton.UseSelectable = true;
            this.TextInmueble.CustomButton.Visible = false;
            this.TextInmueble.Enabled = false;
            this.TextInmueble.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextInmueble.Lines = new string[0];
            this.TextInmueble.Location = new System.Drawing.Point(160, 448);
            this.TextInmueble.MaxLength = 7;
            this.TextInmueble.Name = "TextInmueble";
            this.TextInmueble.PasswordChar = '\0';
            this.TextInmueble.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextInmueble.SelectedText = "";
            this.TextInmueble.SelectionLength = 0;
            this.TextInmueble.SelectionStart = 0;
            this.TextInmueble.ShortcutsEnabled = true;
            this.TextInmueble.Size = new System.Drawing.Size(144, 29);
            this.TextInmueble.TabIndex = 5;
            this.TextInmueble.UseSelectable = true;
            this.TextInmueble.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextInmueble.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(160, 488);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(131, 19);
            this.metroLabel8.TabIndex = 39;
            this.metroLabel8.Text = "Valor Contenido (UF)";
            // 
            // TextContenido
            // 
            this.TextContenido.AccessibleName = "Numero del Plan";
            this.TextContenido.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextContenido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextContenido.CustomButton.Image = null;
            this.TextContenido.CustomButton.Location = new System.Drawing.Point(87, 1);
            this.TextContenido.CustomButton.Name = "";
            this.TextContenido.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextContenido.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.TextContenido.CustomButton.TabIndex = 1;
            this.TextContenido.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextContenido.CustomButton.UseSelectable = true;
            this.TextContenido.CustomButton.Visible = false;
            this.TextContenido.Enabled = false;
            this.TextContenido.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextContenido.Lines = new string[0];
            this.TextContenido.Location = new System.Drawing.Point(160, 512);
            this.TextContenido.MaxLength = 7;
            this.TextContenido.Name = "TextContenido";
            this.TextContenido.PasswordChar = '\0';
            this.TextContenido.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextContenido.SelectedText = "";
            this.TextContenido.SelectionLength = 0;
            this.TextContenido.SelectionStart = 0;
            this.TextContenido.ShortcutsEnabled = true;
            this.TextContenido.Size = new System.Drawing.Size(144, 29);
            this.TextContenido.TabIndex = 6;
            this.TextContenido.UseSelectable = true;
            this.TextContenido.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextContenido.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(344, 424);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(50, 19);
            this.metroLabel9.TabIndex = 40;
            this.metroLabel9.Text = "Region";
            // 
            // ComboRegion
            // 
            this.ComboRegion.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboRegion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboRegion.Enabled = false;
            this.ComboRegion.FormattingEnabled = true;
            this.ComboRegion.ItemHeight = 23;
            this.ComboRegion.Location = new System.Drawing.Point(344, 448);
            this.ComboRegion.Name = "ComboRegion";
            this.ComboRegion.Size = new System.Drawing.Size(144, 29);
            this.ComboRegion.TabIndex = 7;
            this.ComboRegion.UseSelectable = true;
            this.ComboRegion.SelectedIndexChanged += new System.EventHandler(this.ComboRegion_SelectedIndexChanged);
            // 
            // metroLabel11
            // 
            this.metroLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(528, 424);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(59, 19);
            this.metroLabel11.TabIndex = 42;
            this.metroLabel11.Text = "Comuna";
            // 
            // ComboComuna
            // 
            this.ComboComuna.AccessibleName = "Menú Selección Plan a Contratar";
            this.ComboComuna.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboComuna.Enabled = false;
            this.ComboComuna.FormattingEnabled = true;
            this.ComboComuna.ItemHeight = 23;
            this.ComboComuna.Location = new System.Drawing.Point(528, 448);
            this.ComboComuna.Name = "ComboComuna";
            this.ComboComuna.Size = new System.Drawing.Size(144, 29);
            this.ComboComuna.TabIndex = 8;
            this.ComboComuna.UseSelectable = true;
            // 
            // BtnBuscarVivienda
            // 
            this.BtnBuscarVivienda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnBuscarVivienda.Enabled = false;
            this.BtnBuscarVivienda.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnBuscarVivienda.Location = new System.Drawing.Point(344, 320);
            this.BtnBuscarVivienda.Name = "BtnBuscarVivienda";
            this.BtnBuscarVivienda.Size = new System.Drawing.Size(72, 32);
            this.BtnBuscarVivienda.TabIndex = 2;
            this.BtnBuscarVivienda.Text = "Buscar";
            this.BtnBuscarVivienda.UseSelectable = true;
            this.BtnBuscarVivienda.UseStyleColors = true;
            this.BtnBuscarVivienda.Click += new System.EventHandler(this.BtnBuscarVivienda_Click);
            // 
            // BtnAgregarVivienda
            // 
            this.BtnAgregarVivienda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnAgregarVivienda.Enabled = false;
            this.BtnAgregarVivienda.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnAgregarVivienda.Location = new System.Drawing.Point(344, 512);
            this.BtnAgregarVivienda.Name = "BtnAgregarVivienda";
            this.BtnAgregarVivienda.Size = new System.Drawing.Size(144, 32);
            this.BtnAgregarVivienda.TabIndex = 9;
            this.BtnAgregarVivienda.Text = "Agregar";
            this.BtnAgregarVivienda.UseSelectable = true;
            this.BtnAgregarVivienda.UseStyleColors = true;
            this.BtnAgregarVivienda.Click += new System.EventHandler(this.BtnAgregarVivienda_Click);
            // 
            // BtnCancelar1
            // 
            this.BtnCancelar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCancelar1.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCancelar1.Location = new System.Drawing.Point(8, 104);
            this.BtnCancelar1.Name = "BtnCancelar1";
            this.BtnCancelar1.Size = new System.Drawing.Size(144, 32);
            this.BtnCancelar1.TabIndex = 7;
            this.BtnCancelar1.Text = "Volver";
            this.BtnCancelar1.UseSelectable = true;
            this.BtnCancelar1.UseStyleColors = true;
            this.BtnCancelar1.Click += new System.EventHandler(this.BtnVolver_Click);
            // 
            // BtnCargarVehiculo
            // 
            this.BtnCargarVehiculo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCargarVehiculo.Enabled = false;
            this.BtnCargarVehiculo.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCargarVehiculo.Location = new System.Drawing.Point(528, 232);
            this.BtnCargarVehiculo.Name = "BtnCargarVehiculo";
            this.BtnCargarVehiculo.Size = new System.Drawing.Size(144, 32);
            this.BtnCargarVehiculo.TabIndex = 43;
            this.BtnCargarVehiculo.Text = "Cargar Datos";
            this.BtnCargarVehiculo.UseSelectable = true;
            this.BtnCargarVehiculo.UseStyleColors = true;
            this.BtnCargarVehiculo.Click += new System.EventHandler(this.BtnCargarVehiculo_Click);
            // 
            // BtnCargarVivienda
            // 
            this.BtnCargarVivienda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCargarVivienda.Enabled = false;
            this.BtnCargarVivienda.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCargarVivienda.Location = new System.Drawing.Point(528, 512);
            this.BtnCargarVivienda.Name = "BtnCargarVivienda";
            this.BtnCargarVivienda.Size = new System.Drawing.Size(144, 32);
            this.BtnCargarVivienda.TabIndex = 44;
            this.BtnCargarVivienda.Text = "Cargar Datos";
            this.BtnCargarVivienda.UseSelectable = true;
            this.BtnCargarVivienda.UseStyleColors = true;
            this.BtnCargarVivienda.Click += new System.EventHandler(this.BtnCargarVivienda_Click);
            // 
            // AgregarExtraContrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.BtnCargarVivienda);
            this.Controls.Add(this.BtnCargarVehiculo);
            this.Controls.Add(this.BtnCancelar1);
            this.Controls.Add(this.BtnAgregarVivienda);
            this.Controls.Add(this.BtnBuscarVivienda);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.ComboComuna);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.ComboRegion);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.TextContenido);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.TextInmueble);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.TextDireccion);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.ComboAnnoVivienda);
            this.Controls.Add(this.BtnAgregarVehiculo);
            this.Controls.Add(this.BtnBuscarVehiculo);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.ComboAnnoVehiculo);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.ComboModelo);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.ComboMarca);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.TextCodigo);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.TextPatente);
            this.Name = "AgregarExtraContrato";
            this.Text = "Ingreso de Materia Asegurada";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox TextPatente;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox TextCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox ComboMarca;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox ComboModelo;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox ComboAnnoVehiculo;
        private MetroFramework.Controls.MetroButton BtnBuscarVehiculo;
        private MetroFramework.Controls.MetroButton BtnAgregarVehiculo;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox ComboAnnoVivienda;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox TextDireccion;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox TextInmueble;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox TextContenido;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox ComboRegion;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroComboBox ComboComuna;
        private MetroFramework.Controls.MetroButton BtnBuscarVivienda;
        private MetroFramework.Controls.MetroButton BtnAgregarVivienda;
        private MetroFramework.Controls.MetroButton BtnCancelar1;
        private MetroFramework.Controls.MetroButton BtnCargarVehiculo;
        private MetroFramework.Controls.MetroButton BtnCargarVivienda;
    }
}