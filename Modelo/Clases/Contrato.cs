﻿using System;

namespace Modelo.Clases
{
    public class Contrato
    {
        public String Numero { get; set; }
        public String FechaCreacion { get; set; }
        public String FechaTermino { get; set; }
        public String RutCliente { get; set; }
        public String CodigoPlan { get; set; }
        public Int32 IdTipoContrato { get; set; }
        public String FechaInicioVigencia { get; set; }
        public String FechaFinVigencia { get; set; }
        public Int32 DeclaracionSalud { get; set; }
        public Double PrimaAnual { get; set; }
        public Double PrimaMensual { get; set; }
        public String Observaciones { get; set; }
    }
}