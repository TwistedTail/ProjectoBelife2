﻿using System;

namespace Modelo.Clases
{
    public class MarcaModeloVehiculo
    {
        public String MarcaVehiculo { get; set; }
        public String ModeloVehiculo { get; set; }
    }
}
