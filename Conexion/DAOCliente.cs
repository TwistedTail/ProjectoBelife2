﻿using Oracle.DataAccess.Client;
using Modelo.Clases;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOCliente
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteCliente(RutCliente)
        public Boolean ExisteCliente(String RutCliente)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(RutCliente) from cliente where RutCliente = :RutCliente"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region TieneContratos(RutCliente)
        public Boolean TieneContratos(String RutCliente)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(nombre) from contrato where RutCliente = :RutCliente"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region AgregarCliente(RutCliente,Nombres,Apellidos,FechaNacimiento,IdSexo,IdEstadoCivil)
        public void AgregarCliente(String RutCliente, String Nombres, String Apellidos, String FechaNacimiento, Int32 IdSexo, Int32 IdEstadoCivil)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "insert into cliente values (:RutCliente, :Nombres, :Apellidos, :FechaNacimiento, :IdSexo, :IdEstadoCivil)"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.Parameters.Add(new OracleParameter("Nombres", Nombres));
            Comando.Parameters.Add(new OracleParameter("Apellidos", Apellidos));
            Comando.Parameters.Add(new OracleParameter("FechaNacimiento", FechaNacimiento));
            Comando.Parameters.Add(new OracleParameter("IdSexo", IdSexo));
            Comando.Parameters.Add(new OracleParameter("IdEstadoCivil", IdEstadoCivil));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion

        #region ActualizarCliente(RutCliente,Nombres,Apellidos,FechaNacimiento,IdSexo,IdEstadoCivil)
        public void ActualizarCliente(String RutCliente, String Nombres, String Apellidos, String FechaNacimiento, Int32 IdSexo, Int32 IdEstadoCivil)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "update cliente set Nombres = :Nombres, Apellidos = :Apellidos, FechaNacimiento = :FechaNacimiento, IdSexo = :IdSexo, IdEstadoCivil = :IdEstadoCivil where RutCliente = :RutCliente"
            };
            Comando.Parameters.Add(new OracleParameter("Nombres", Nombres));
            Comando.Parameters.Add(new OracleParameter("Apellidos", Apellidos));
            Comando.Parameters.Add(new OracleParameter("FechaNacimiento", FechaNacimiento));
            Comando.Parameters.Add(new OracleParameter("IdSexo", IdSexo));
            Comando.Parameters.Add(new OracleParameter("IdEstadoCivil", IdEstadoCivil));
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion

        #region EliminarCliente(RutCliente)
        public Boolean EliminarCliente(String RutCliente)
        {
            if (!ExisteCliente(RutCliente)) { return false; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "delete from cliente where RutCliente = :RutCliente"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();

            return true;
        }
        #endregion

        #region GetCliente(RutCliente)
        public Cliente GetCliente(String RutCliente)
        {
            if (!ExisteCliente(RutCliente)) { return new Cliente(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select RutCliente, Nombres, Apellidos, FechaNacimiento, IdSexo, IdEstadoCivil from cliente where RutCliente = :RutCliente"
            };
            Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            OracleDataReader Reader = Comando.ExecuteReader();

            Cliente Result = new Cliente();
            while (Reader.Read())
            {
                Result = new Cliente()
                {
                    RutCliente = Reader["RutCliente"].ToString(),
                    Nombres = Reader["Nombres"].ToString(),
                    Apellidos = Reader["Apellidos"].ToString(),
                    FechaNacimiento = Reader["FechaNacimiento"].ToString(),
                    IdSexo = Convert.ToInt32(Reader["IdSexo"]),
                    IdEstadoCivil = Convert.ToInt32(Reader["IdEstadoCivil"])
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllClientes()
        public List<Cliente> GetAllClientes()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select RutCliente, Nombres, Apellidos, FechaNacimiento, IdSexo, IdEstadoCivil from cliente"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Cliente> Result = new List<Cliente>();
            while (Reader.Read())
            {
                Cliente c = new Cliente
                {
                    RutCliente = Reader["RutCliente"].ToString(),
                    Nombres = Reader["Nombres"].ToString(),
                    Apellidos = Reader["Apellidos"].ToString(),
                    FechaNacimiento = Reader["FechaNacimiento"].ToString(),
                    IdSexo = Convert.ToInt32(Reader["IdSexo"]),
                    IdEstadoCivil = Convert.ToInt32(Reader["IdEstadoCivil"])
                };
                Result.Add(c);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetClientes(RutCliente,IdSexo,IdEstadoCivil)
        public List<Cliente> GetClientes(String RutCliente, Int32 IdSexo, Int32 IdEstadoCivil)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion()
            };

            String CommandText = "select RutCliente, Nombres, Apellidos, FechaNacimiento, IdSexo, IdEstadoCivil from cliente";

            if (!String.IsNullOrWhiteSpace(RutCliente) & !String.IsNullOrEmpty(RutCliente))
            {
                CommandText += " where RutCliente = :RutCliente";
                Comando.Parameters.Add(new OracleParameter("RutCliente", RutCliente));
            }
            else if (IdSexo > 0)
            {
                CommandText += " where IdSexo = :IdSexo";
                Comando.Parameters.Add(new OracleParameter("IdSexo", IdSexo));
            }
            else if (IdEstadoCivil > 0)
            {
                    CommandText += " where IdEstadoCivil = :IdEstadoCivil";
                    Comando.Parameters.Add(new OracleParameter("IdEstadoCivil", IdEstadoCivil));
            }

            Comando.CommandText = CommandText;
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Cliente> Result = new List<Cliente>();
            while (Reader.Read())
            {
                Cliente c = new Cliente()
                {
                    RutCliente = Reader["RutCliente"].ToString(),
                    Nombres = Reader["Nombres"].ToString(),
                    Apellidos = Reader["Apellidos"].ToString(),
                    FechaNacimiento = Reader["FechaNacimiento"].ToString(),
                    IdSexo = Convert.ToInt32(Reader["IdSexo"]),
                    IdEstadoCivil = Convert.ToInt32(Reader["IdEstadoCivil"])
                };
                Result.Add(c);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}