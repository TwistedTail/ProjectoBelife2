﻿using Conexion;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Modelo.Clases;
using Modelo.Recursos;
using System;
using System.Linq;

namespace Vista
{
    public partial class AgregarExtraContrato : MetroForm
    {
        private AgregarContrato Window { get; set; }
        private String MarcaActual { get; set; }
        private String RegionActual { get; set; }

        #region Constructor AgregarExtraContrato()
        public AgregarExtraContrato()
        {
            InitializeComponent();

            ComboAnnoVehiculo.Items.Add("-N/A-");
            ComboAnnoVivienda.Items.Add("-N/A-");
            ComboAnnoVehiculo.SelectedIndex = 0;
            ComboAnnoVivienda.SelectedIndex = 0;

            for (Int32 I = DateTime.Today.Year; I >= 1910; I--)
            {
                ComboAnnoVehiculo.Items.Add(I);
                ComboAnnoVivienda.Items.Add(I);
            }

            DAOMarca Marcas = new DAOMarca();
            ComboMarca.Items.Add("-N/A-");
            ComboMarca.SelectedIndex = 0;
            Marcas.GetAllMarcas().ForEach(delegate (MarcaVehiculo M) { ComboMarca.Items.Add(M.Descripcion); });

            DAORegion Regiones = new DAORegion();
            ComboRegion.Items.Add("-N/A-");
            ComboRegion.SelectedIndex = 0;
            Regiones.GetAllRegiones().ForEach(delegate (Region R) { ComboRegion.Items.Add(R.NombreRegion); });
        }
        #endregion

        #region HabilitarControlesVehiculo(AgregarContrato)
        public void HabilitarControlesVehiculo(AgregarContrato NewWindow)
        {
            Window = NewWindow;

            TextPatente.Enabled = true;
            BtnBuscarVehiculo.Enabled = true;
            ComboMarca.Enabled = true;
            ComboModelo.Enabled = true;
            ComboAnnoVehiculo.Enabled = true;
            BtnAgregarVehiculo.Enabled = true;
            BtnCargarVehiculo.Enabled = true;
        }
        #endregion

        #region HabilitarControlesVivienda(AgregarContrato)
        public void HabilitarControlesVivienda(AgregarContrato NewWindow)
        {
            Window = NewWindow;

            TextCodigo.Enabled = true;
            BtnBuscarVivienda.Enabled = true;
            ComboAnnoVivienda.Enabled = true;
            TextDireccion.Enabled = true;
            TextInmueble.Enabled = true;
            TextContenido.Enabled = true;
            ComboRegion.Enabled = true;
            ComboComuna.Enabled = true;
            BtnAgregarVivienda.Enabled = true;
            BtnCargarVivienda.Enabled = true;
        }
        #endregion

        #region LLenarDatosVehiculo(Vehiculo)
        private void LlenarDatosVehiculo(Vehiculo Vehiculo)
        {
            DAOMarca Marcas = new DAOMarca();
            DAOModelo Modelos = new DAOModelo();

            ComboMarca.SelectedItem = Marcas.GetMarca(Vehiculo.IdMarca).Descripcion;
            ActualizarModelos();
            ComboModelo.SelectedItem = Modelos.GetModelo(Vehiculo.IdModelo).Descripcion;
            ComboAnnoVehiculo.SelectedItem = Vehiculo.Anno;
        }
        #endregion

        #region LLenarDatosVivienda(Vivienda)
        private void LlenarDatosVivienda(Vivienda Vivienda)
        {
            DAORegion Regiones = new DAORegion();
            DAOComuna Comunas = new DAOComuna();

            ComboAnnoVivienda.SelectedItem = Vivienda.Anno;
            TextDireccion.Text = Vivienda.Direccion;
            TextInmueble.Text = Vivienda.ValorInmueble.ToString();
            TextContenido.Text = Vivienda.ValorContenido.ToString();
            ComboRegion.SelectedItem = Regiones.GetRegion(Vivienda.IdRegion).NombreRegion;
            ActualizarComunas();
            ComboComuna.SelectedItem = Comunas.GetComuna(Vivienda.IdComuna).NombreComuna;
        }
        #endregion

        #region LimpiarDatos()
        private void LimpiarDatos()
        {
            ComboMarca.SelectedIndex = 0;
            ComboAnnoVehiculo.SelectedIndex = 0;

            ComboAnnoVivienda.SelectedIndex = 0;
            TextDireccion.Text = "";
            TextInmueble.Text = "";
            TextContenido.Text = "";
            ComboRegion.SelectedIndex = 0;
        }
        #endregion

        #region ActualizarModelos()
        private void ActualizarModelos()
        {
            ComboModelo.Items.Clear();
            ComboModelo.Items.Add("-N/A-");
            ComboModelo.SelectedIndex = 0;

            if (ComboMarca.SelectedIndex == 0) { return; }

            DAOMarca Marcas = new DAOMarca();
            DAOModelo Modelos = new DAOModelo();

            Int32 Marca = Marcas.GetMarca(MarcaActual).IdMarca;

            Modelos.GetModelos(Marca).ForEach(delegate (ModeloVehiculo M) { ComboModelo.Items.Add(M.Descripcion); });
        }
        #endregion

        #region ActualizarComunas()
        private void ActualizarComunas()
        {
            ComboComuna.Items.Clear();
            ComboComuna.Items.Add("-N/A-");
            ComboComuna.SelectedIndex = 0;

            if (ComboRegion.SelectedIndex == 0) { return; }

            DAORegion Regiones = new DAORegion();
            DAOComuna Comunas = new DAOComuna();

            Int32 Region = Regiones.GetRegion(RegionActual).IdRegion;

            Comunas.GetComunas(Region).ForEach(delegate (Comuna C) { ComboComuna.Items.Add(C.NombreComuna); });
        }
        #endregion

        #region Click BtnBuscarVivienda
        private void BtnBuscarVehiculo_Click(Object sender, EventArgs e)
        {
            DAOVehiculo Vehiculos = new DAOVehiculo();
            String Patente = TextPatente.Text;

            if (String.IsNullOrEmpty(Patente) | String.IsNullOrWhiteSpace(Patente)) { return; }

            if (!Vehiculos.ExisteVehiculo(Patente))
            {
                MetroMessageBox.Show(this, "Patente Ingresada no Existe", "Error");
                return;
            }

            LlenarDatosVehiculo(Vehiculos.GetVehiculo(Patente));
        }
        #endregion

        #region Click BtnBuscarVivienda
        private void BtnBuscarVivienda_Click(Object sender, EventArgs e)
        {
            //Agrega un metodo LlenarDatosVivienda(Vivienda V)
            DAOVivienda Viviendas = new DAOVivienda();
            String CodigoPostal = TextCodigo.Text;

            if (String.IsNullOrEmpty(CodigoPostal) | String.IsNullOrWhiteSpace(CodigoPostal)) { return; }
            if (!Int32.TryParse(TextCodigo.Text, out Int32 Codigo)) { return; }

            if (!Viviendas.ExisteVivienda(Codigo))
            {
                MetroMessageBox.Show(this, "Codigo Postal Ingresado no Existe", "Error");
                return;
            }

            LlenarDatosVivienda(Viviendas.GetVivienda(Codigo));
        }
        #endregion

        #region Selection Changed ComboMarca
        private void ComboMarca_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Marca = (sender as MetroComboBox).Text;

            if (Marca == null) { return; }

            MarcaActual = Marca;
            ActualizarModelos();
        }
        #endregion

        #region Click BtnAgregarVehiculo
        private void BtnAgregarVehiculo_Click(Object sender, EventArgs e)
        {
            Boolean HasPatente = !String.IsNullOrEmpty(TextPatente.Text) & !String.IsNullOrWhiteSpace(TextPatente.Text);
            Boolean HasModelo = ComboModelo.SelectedIndex > 0;
            Boolean HasAnno = ComboAnnoVehiculo.SelectedIndex > 0;

            if (!(HasPatente & HasModelo & HasAnno))
            {
                MetroMessageBox.Show(this, "Falta información por llenar en el formulario.", "Faltan Datos");
                return;
            }

            ValidadorPatente Validador = new ValidadorPatente();

            if (!Validador.ValidarPatente(TextPatente.Text))
            {
                MetroMessageBox.Show(this, "Formato de la Patente Ingresada No Es Valido", "Error de Formato");
                return;
            }

            DAOVehiculo Vehiculos = new DAOVehiculo();
            DAOMarca Marcas = new DAOMarca();
            DAOModelo Modelos = new DAOModelo();

            Int32 Marca = Marcas.GetMarca(ComboMarca.Text).IdMarca;
            Int32 Modelo = Modelos.GetModelo(ComboModelo.Text).IdModelo;
            Int32 Anno = Convert.ToInt32(ComboAnnoVehiculo.Text);

            String Mensaje = "";
            if (Vehiculos.ExisteVehiculo(TextPatente.Text))
            {
                Vehiculos.ActualizarVehiculo(TextPatente.Text, Marca, Modelo, Anno);
                Mensaje = "Datos del Vehiculo Actualizados Correctamente";
            }
            else
            {
                Vehiculos.AgregarVehiculo(TextPatente.Text, Marca, Modelo, Anno);
                Mensaje = "Nuevo Vehiculo Ingresado Correctamente";
            }

            MetroMessageBox.Show(this, Mensaje, "Proceso Correcto");
            LimpiarDatos();
        }
        #endregion

        #region Click BtnAgregarVivienda
        private void BtnAgregarVivienda_Click(Object sender, EventArgs e)
        {
            Boolean HasCodigo = !String.IsNullOrEmpty(TextCodigo.Text) & !String.IsNullOrWhiteSpace(TextCodigo.Text);
            Boolean HasAnno = ComboAnnoVivienda.SelectedIndex > 0;
            Boolean HasDireccion = !String.IsNullOrEmpty(TextDireccion.Text) & !String.IsNullOrWhiteSpace(TextDireccion.Text);
            Boolean HasComuna = ComboComuna.SelectedIndex > 0;
            Boolean HasInmueble = !String.IsNullOrEmpty(TextInmueble.Text) & !String.IsNullOrWhiteSpace(TextInmueble.Text);
            Boolean HasContenido = !String.IsNullOrEmpty(TextContenido.Text) & !String.IsNullOrWhiteSpace(TextContenido.Text);

            if (!(HasCodigo & HasAnno & HasDireccion & HasComuna & HasInmueble & HasContenido))
            {
                MetroMessageBox.Show(this, "Falta información por llenar en el formulario.", "Faltan Datos");
                return;
            }

            if (!Int32.TryParse(TextCodigo.Text, out Int32 Codigo))
            {
                MetroMessageBox.Show(this, "El Codigo Postal ingresado no es un valor numerico", "Codigo Postal No Valido");
                return;
            }

            if (!Enumerable.Range(1000000, 9999999).Contains(Codigo))
            {
                MetroMessageBox.Show(this, "El Codigo Postal ingresado esta fuera del rango permitido", "Codigo Postal No Valido");
                return;
            }

            if (!Double.TryParse(TextInmueble.Text, out Double Inmueble) | !Double.TryParse(TextContenido.Text, out Double Contenido))
            {
                MetroMessageBox.Show(this, "Se han ingresado valores no numericos en los campos de costo en UFs", "Valores No Validos");
                return;
            }

            DAOVivienda Viviendas = new DAOVivienda();
            DAORegion Regiones = new DAORegion();
            DAOComuna Comunas = new DAOComuna();

            Int32 Region = Regiones.GetRegion(ComboRegion.Text).IdRegion;
            Int32 Comuna = Comunas.GetComuna(ComboComuna.Text).IdComuna;
            Int32 Anno = Convert.ToInt32(ComboAnnoVivienda.Text);
            String Direccion = TextDireccion.Text;
            String Titulo;
            String Mensaje;

            if (Viviendas.ExisteVivienda(Codigo))
            {
                Viviendas.ActualizarVivienda(Codigo, Anno, Direccion, Inmueble, Contenido, Region, Comuna);
                Titulo = "Vivienda Actualizada!";
                Mensaje = "Se ha actualizado una vivienda exitosamente.";
            }
            else
            {
                Viviendas.AgregarVivienda(Codigo, Anno, Direccion, Inmueble, Contenido, Region, Comuna);
                Titulo = "Vivienda Agregada!";
                Mensaje = "Se ha agregado una vivienda exitosamente.";
            }

            MetroMessageBox.Show(this, Mensaje, Titulo);
            LimpiarDatos();
        }
        #endregion

        #region Click BtnCargarVehiculo
        private void BtnCargarVehiculo_Click(Object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TextPatente.Text) | String.IsNullOrWhiteSpace(TextPatente.Text)) { return; }

            DAOVehiculo Vehiculos = new DAOVehiculo();

            if (!Vehiculos.ExisteVehiculo(TextPatente.Text)) { return; }

            Window.CargarExtra(TextPatente.Text);
            Close();
            Dispose();
        }
        #endregion

        #region Click BtnCargarVivienda
        private void BtnCargarVivienda_Click(Object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TextCodigo.Text) | String.IsNullOrWhiteSpace(TextCodigo.Text)) { return; }

            DAOVivienda Viviendas = new DAOVivienda();

            if (!Viviendas.ExisteVivienda(Convert.ToInt32(TextCodigo.Text))) { return; }

            Window.CargarExtra(TextPatente.Text);
            Close();
            Dispose();
        }
        #endregion

        #region Selection Changed ComboRegion
        private void ComboRegion_SelectedIndexChanged(Object sender, EventArgs e)
        {
            String Region = (sender as MetroComboBox).Text;

            if (Region == null) { return; }

            RegionActual = Region;
            ActualizarComunas();
        }
        #endregion

        #region Click BtnVolver
        private void BtnVolver_Click(Object sender, EventArgs e)
        {
            Window.ActualizarPlanes();
            Close();
            Dispose();
        }
        #endregion
    }
}