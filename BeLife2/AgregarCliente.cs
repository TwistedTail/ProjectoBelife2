﻿using Conexion;
using MetroFramework;
using MetroFramework.Forms;
using Modelo.Clases;
using Modelo.Recursos;
using System;

namespace Vista
{
    public partial class AgregarCliente : MetroForm
    {
        #region Constructor AgregarCliente
        public AgregarCliente()
        {
            InitializeComponent();

            TimePickerNacimiento.MinDate = new DateTime(1900,1,1);
            TimePickerNacimiento.MaxDate = DateTime.Today.AddYears(-18);

            DAOSexo Sexos = new DAOSexo();
            ComboSexo.Items.Add("-N/A-");
            Sexos.GetAllSexos().ForEach(delegate (Sexo S) { ComboSexo.Items.Add(S.Descripcion); });
            ComboSexo.SelectedIndex = 0;

            DAOEstadoCivil Estados = new DAOEstadoCivil();
            ComboEstCivil.Items.Add("-N/A-");
            Estados.GetAllEstadoCivil().ForEach(delegate (EstadoCivil E) { ComboEstCivil.Items.Add(E.Descripcion); });
            ComboEstCivil.SelectedIndex = 0;
        }
        #endregion

        #region LimpiarDatos()
        private void LimpiarDatos()
        {
            TextRut.Text = "";
            TextNombres.Text = "";
            TextApellidos.Text = "";

            TimePickerNacimiento.Value = TimePickerNacimiento.MaxDate;

            ComboSexo.SelectedIndex = 0;
            ComboEstCivil.SelectedIndex = 0;
        }
        #endregion

        #region LlenarDatos(Cliente)
        public void LlenarDatos(Cliente c)
        {
            TextRut.Text = c.RutCliente;
            TextNombres.Text = c.Nombres;
            TextApellidos.Text = c.Apellidos;

            TimePickerNacimiento.Text = c.FechaNacimiento;

            ComboSexo.SelectedIndex = c.IdSexo;
            ComboEstCivil.SelectedIndex = c.IdEstadoCivil;
        }
        #endregion

        #region Click BtnBuscar
        private void BtnBuscar_Click(Object sender, EventArgs e)
        {
            DAOCliente Clientes = new DAOCliente();
            String Rut = TextRut.Text;

            if (String.IsNullOrEmpty(Rut) | String.IsNullOrWhiteSpace(Rut)) { return; }

            if (!Clientes.ExisteCliente(Rut))
            {
                MetroMessageBox.Show(this, "RUT Ingresado No Existe", "Error");
                return;
            }

            LlenarDatos(Clientes.GetCliente(Rut));
        }
        #endregion

        #region Click BtnListar
        private void BtnListar_Click(Object sender, EventArgs e)
        {
            ListarCliente Ventana = new ListarCliente();
            StyleManager.Clone(Ventana);
            Ventana.MostrarBotonCargar(this);
            Ventana.ShowDialog();
        }
        #endregion

        #region Click BtnIngresar
        private void BtnIngresar_Click(Object sender, EventArgs e)
        {
            Boolean HasRut = !String.IsNullOrEmpty(TextRut.Text) & !String.IsNullOrWhiteSpace(TextRut.Text);
            Boolean HasNombres = !String.IsNullOrEmpty(TextNombres.Text) & !String.IsNullOrWhiteSpace(TextNombres.Text);
            Boolean HasApellidos = !String.IsNullOrEmpty(TextApellidos.Text) & !String.IsNullOrWhiteSpace(TextApellidos.Text);
            Boolean HasSexo = ComboSexo.SelectedIndex > 0;
            Boolean HasEstado = ComboEstCivil.SelectedIndex > 0;
            Boolean HasAllData = HasRut & HasNombres & HasApellidos & HasSexo & HasEstado;

            if (!HasAllData)
            {
                MetroMessageBox.Show(this, "Falta información por llenar en el formulario.", "Faltan Datos");
                return;
            }

            ValidadorRUT Validador = new ValidadorRUT();

            if (!Validador.ValidarRut(TextRut.Text))
            {
                MetroMessageBox.Show(this, "El RUT ingresado no posee un formato valido o es incorrecto", "RUT No Valido");
                return;
            }

            DAOCliente Clientes = new DAOCliente();

            String Rut = TextRut.Text;
            String Nombres = TextNombres.Text;
            String Apellidos = TextApellidos.Text;
            String Nacimiento = TimePickerNacimiento.Value.ToShortDateString();
            Int32 Sexo = ComboSexo.SelectedIndex;
            Int32 Estado = ComboEstCivil.SelectedIndex;

            String Mensaje = "";

            if (Clientes.ExisteCliente(Rut))
            {
                Clientes.ActualizarCliente(Rut, Nombres, Apellidos, Nacimiento, Sexo, Estado);
                Mensaje = "Cliente actualizado exitosamente.";
            }
            else
            {
                Clientes.AgregarCliente(Rut, Nombres, Apellidos, Nacimiento, Sexo, Estado);
                Mensaje = "Cliente ingresado exitosamente.";
            }

            MetroMessageBox.Show(this, Mensaje, "Proceso Exitoso");
            LimpiarDatos();
        }
        #endregion

        #region Click BtnEliminar
        private void BtnEliminar_Click(Object sender, EventArgs e)
        {
            String Rut = TextRut.Text;

            if (String.IsNullOrEmpty(Rut) | String.IsNullOrWhiteSpace(Rut)) { return; }

            DAOCliente Clientes = new DAOCliente();

            if (!Clientes.ExisteCliente(Rut))
            {
                MetroMessageBox.Show(this, "RUT de Cliente ingresado no existe.", "Error al Eliminar");
                return;
            }
            else if (Clientes.TieneContratos(Rut))
            {
                MetroMessageBox.Show(this, "RUT de Cliente ingresado posee contratos asociados.", "Error al Eliminar");
                return;
            }

            Clientes.EliminarCliente(Rut);
            MetroMessageBox.Show(this, "Cliente ingresado eliminado exitosamente.", "Proceso Exitoso");
            LimpiarDatos();
        }
        #endregion

        #region Click BtnSalir
        private void BtnSalir_Click(Object sender, EventArgs e)
        {
            Dispose();
            Close();
        }
        #endregion
    }
}