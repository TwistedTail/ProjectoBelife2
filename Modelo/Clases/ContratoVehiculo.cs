﻿using System;

namespace Modelo.Clases
{
    public class ContratoVehiculo
    {
        public String Numero { get; set; }
        public String Patente { get; set; }
    }
}