﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOMarca
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteMarca(IdMarca)
        public Boolean ExisteMarca(Int32 IdMarca)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdMarca) from marcaVehiculo where IdMarca = :IdMarca"
            };
            Comando.Parameters.Add(new OracleParameter("IdMarca", IdMarca));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteMarca(Descripcion)
        public Boolean ExisteMarca(String Descripcion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdMarca) from marcaVehiculo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetMarca(IdMarca)
        public MarcaVehiculo GetMarca(Int32 IdMarca)
        {
            if (!ExisteMarca(IdMarca)) { return new MarcaVehiculo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Descripcion from marcaVehiculo where IdMarca = :IdMarca"
            };
            Comando.Parameters.Add(new OracleParameter("IdMarca", IdMarca));
            OracleDataReader Reader = Comando.ExecuteReader();

            MarcaVehiculo Result = null;
            while (Reader.Read())
            {
                Result = new MarcaVehiculo
                {
                    IdMarca = IdMarca,
                    Descripcion = Reader["Descripcion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetMarca(Descripcion)
        public MarcaVehiculo GetMarca(String Descripcion)
        {
            if (!ExisteMarca(Descripcion)) { return new MarcaVehiculo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdMarca from marcaVehiculo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            OracleDataReader Reader = Comando.ExecuteReader();

            MarcaVehiculo Result = null;
            while (Reader.Read())
            {
                Result = new MarcaVehiculo
                {
                    IdMarca = Convert.ToInt32(Reader["IdMarca"]),
                    Descripcion = Descripcion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllMarcas()
        public List<MarcaVehiculo> GetAllMarcas()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdMarca, Descripcion from marcaVehiculo"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<MarcaVehiculo> Result = new List<MarcaVehiculo>();
            while (Reader.Read())
            {
                MarcaVehiculo m = new MarcaVehiculo
                {
                    IdMarca = Convert.ToInt32(Reader["IdMarca"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(m);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}