﻿using Oracle.DataAccess.Client;

namespace Conexion
{
    public class ConexionDB
    {
        private OracleConnection myConexion;

        #region Constructor ConexionDB
        public ConexionDB()
        {
            myConexion = new OracleConnection(string.Format("Data Source={0};User Id={1};Password={2};", "localhost", "belife", "belife"));
            myConexion.Open();
        }
        #endregion

        #region GetConexion()
        public OracleConnection GetConexion() => myConexion;
        #endregion

        #region CloseConexion()
        public void CloseConexion()
        {
            myConexion.Close();
            myConexion.Dispose();
        }
        #endregion
    }
}