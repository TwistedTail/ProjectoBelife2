﻿using System;

namespace Modelo.Clases
{
    public class Comuna
    {
        public Int32 IdComuna { get; set; }
        public String NombreComuna { get; set; }
    }
}