﻿using System;

namespace Modelo.Clases
{
    public class Vivienda
    {
        public Int32 CodigoPostal { get; set; }
        public Int32 Anno { get; set; }
        public String Direccion { get; set; }
        public Double ValorInmueble { get; set; }
        public Double ValorContenido { get; set; }
        public Int32 IdRegion { get; set; }
        public Int32 IdComuna { get; set; }
    }
}