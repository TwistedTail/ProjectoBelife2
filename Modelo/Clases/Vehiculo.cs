﻿using System;

namespace Modelo.Clases
{
    public class Vehiculo
    {
        public String Patente { get; set; }
        public Int32 IdMarca { get; set; }
        public Int32 IdModelo { get; set; }
        public Int32 Anno { get; set; }
    }
}