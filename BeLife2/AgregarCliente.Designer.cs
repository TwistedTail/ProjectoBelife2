﻿namespace Vista
{
    partial class AgregarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgregarCliente));
            this.TextRut = new MetroFramework.Controls.MetroTextBox();
            this.TextNombres = new MetroFramework.Controls.MetroTextBox();
            this.BtnSalir = new MetroFramework.Controls.MetroButton();
            this.TimePickerNacimiento = new MetroFramework.Controls.MetroDateTime();
            this.TextApellidos = new MetroFramework.Controls.MetroTextBox();
            this.ComboSexo = new MetroFramework.Controls.MetroComboBox();
            this.ComboEstCivil = new MetroFramework.Controls.MetroComboBox();
            this.BtnIngresar = new MetroFramework.Controls.MetroButton();
            this.BtnEliminar = new MetroFramework.Controls.MetroButton();
            this.BtnBuscar = new MetroFramework.Controls.MetroButton();
            this.BtnListar = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // TextRut
            // 
            this.TextRut.AccessibleName = "RUT del Cliente";
            this.TextRut.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextRut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // 
            // 
            this.TextRut.CustomButton.Image = null;
            this.TextRut.CustomButton.Location = new System.Drawing.Point(171, 1);
            this.TextRut.CustomButton.Name = "";
            this.TextRut.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextRut.CustomButton.Style = MetroFramework.MetroColorStyle.Purple;
            this.TextRut.CustomButton.TabIndex = 1;
            this.TextRut.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextRut.CustomButton.UseSelectable = true;
            this.TextRut.CustomButton.Visible = false;
            this.TextRut.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextRut.Lines = new string[0];
            this.TextRut.Location = new System.Drawing.Point(160, 168);
            this.TextRut.MaxLength = 10;
            this.TextRut.Name = "TextRut";
            this.TextRut.PasswordChar = '\0';
            this.TextRut.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextRut.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextRut.SelectedText = "";
            this.TextRut.SelectionLength = 0;
            this.TextRut.SelectionStart = 0;
            this.TextRut.ShortcutsEnabled = true;
            this.TextRut.Size = new System.Drawing.Size(256, 29);
            this.TextRut.TabIndex = 1;
            this.TextRut.UseSelectable = true;
            this.TextRut.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextRut.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // TextNombres
            // 
            this.TextNombres.AccessibleName = "Nombres del Cliente";
            this.TextNombres.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextNombres.CustomButton.Image = null;
            this.TextNombres.CustomButton.Location = new System.Drawing.Point(171, 1);
            this.TextNombres.CustomButton.Name = "";
            this.TextNombres.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextNombres.CustomButton.Style = MetroFramework.MetroColorStyle.Purple;
            this.TextNombres.CustomButton.TabIndex = 1;
            this.TextNombres.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextNombres.CustomButton.UseSelectable = true;
            this.TextNombres.CustomButton.Visible = false;
            this.TextNombres.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextNombres.Lines = new string[0];
            this.TextNombres.Location = new System.Drawing.Point(160, 232);
            this.TextNombres.MaxLength = 20;
            this.TextNombres.Name = "TextNombres";
            this.TextNombres.PasswordChar = '\0';
            this.TextNombres.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextNombres.SelectedText = "";
            this.TextNombres.SelectionLength = 0;
            this.TextNombres.SelectionStart = 0;
            this.TextNombres.ShortcutsEnabled = true;
            this.TextNombres.Size = new System.Drawing.Size(256, 29);
            this.TextNombres.TabIndex = 4;
            this.TextNombres.UseSelectable = true;
            this.TextNombres.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextNombres.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // BtnSalir
            // 
            this.BtnSalir.AccessibleName = "Botón Volver";
            this.BtnSalir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSalir.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnSalir.Location = new System.Drawing.Point(64, 168);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(56, 32);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.Text = "Volver";
            this.BtnSalir.UseSelectable = true;
            this.BtnSalir.UseStyleColors = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // TimePickerNacimiento
            // 
            this.TimePickerNacimiento.AccessibleName = "Selección Fecha Nacimiento del Cliente";
            this.TimePickerNacimiento.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TimePickerNacimiento.DisplayFocus = true;
            this.TimePickerNacimiento.Location = new System.Drawing.Point(160, 296);
            this.TimePickerNacimiento.MinimumSize = new System.Drawing.Size(0, 29);
            this.TimePickerNacimiento.Name = "TimePickerNacimiento";
            this.TimePickerNacimiento.Size = new System.Drawing.Size(256, 30);
            this.TimePickerNacimiento.Style = MetroFramework.MetroColorStyle.Purple;
            this.TimePickerNacimiento.TabIndex = 6;
            // 
            // TextApellidos
            // 
            this.TextApellidos.AccessibleName = "Apellidos del Cliente";
            this.TextApellidos.Anchor = System.Windows.Forms.AnchorStyles.None;
            // 
            // 
            // 
            this.TextApellidos.CustomButton.Image = null;
            this.TextApellidos.CustomButton.Location = new System.Drawing.Point(171, 1);
            this.TextApellidos.CustomButton.Name = "";
            this.TextApellidos.CustomButton.Size = new System.Drawing.Size(20, 22);
            this.TextApellidos.CustomButton.Style = MetroFramework.MetroColorStyle.Purple;
            this.TextApellidos.CustomButton.TabIndex = 1;
            this.TextApellidos.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.TextApellidos.CustomButton.UseSelectable = true;
            this.TextApellidos.CustomButton.Visible = false;
            this.TextApellidos.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.TextApellidos.Lines = new string[0];
            this.TextApellidos.Location = new System.Drawing.Point(456, 232);
            this.TextApellidos.MaxLength = 20;
            this.TextApellidos.Name = "TextApellidos";
            this.TextApellidos.PasswordChar = '\0';
            this.TextApellidos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TextApellidos.SelectedText = "";
            this.TextApellidos.SelectionLength = 0;
            this.TextApellidos.SelectionStart = 0;
            this.TextApellidos.ShortcutsEnabled = true;
            this.TextApellidos.Size = new System.Drawing.Size(256, 29);
            this.TextApellidos.TabIndex = 5;
            this.TextApellidos.UseSelectable = true;
            this.TextApellidos.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.TextApellidos.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // ComboSexo
            // 
            this.ComboSexo.AccessibleName = "Menú Seleccion Sexo del Cliente";
            this.ComboSexo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboSexo.FormattingEnabled = true;
            this.ComboSexo.ItemHeight = 23;
            this.ComboSexo.Location = new System.Drawing.Point(456, 296);
            this.ComboSexo.Name = "ComboSexo";
            this.ComboSexo.Size = new System.Drawing.Size(256, 29);
            this.ComboSexo.TabIndex = 7;
            this.ComboSexo.UseSelectable = true;
            // 
            // ComboEstCivil
            // 
            this.ComboEstCivil.AccessibleName = "Menú Seleccion Estado Civil del Cliente";
            this.ComboEstCivil.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboEstCivil.FormattingEnabled = true;
            this.ComboEstCivil.ItemHeight = 23;
            this.ComboEstCivil.Location = new System.Drawing.Point(160, 360);
            this.ComboEstCivil.Name = "ComboEstCivil";
            this.ComboEstCivil.Size = new System.Drawing.Size(256, 29);
            this.ComboEstCivil.TabIndex = 8;
            this.ComboEstCivil.UseSelectable = true;
            // 
            // BtnIngresar
            // 
            this.BtnIngresar.AccessibleName = "Botón Ingresar o Actualizar Cliente";
            this.BtnIngresar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnIngresar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnIngresar.Location = new System.Drawing.Point(296, 424);
            this.BtnIngresar.Name = "BtnIngresar";
            this.BtnIngresar.Size = new System.Drawing.Size(120, 56);
            this.BtnIngresar.TabIndex = 9;
            this.BtnIngresar.Text = "Ingresar Cliente";
            this.BtnIngresar.UseSelectable = true;
            this.BtnIngresar.UseStyleColors = true;
            this.BtnIngresar.Click += new System.EventHandler(this.BtnIngresar_Click);
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.AccessibleName = "Botón Eliminar Cliente";
            this.BtnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnEliminar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnEliminar.Location = new System.Drawing.Point(456, 424);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.Size = new System.Drawing.Size(120, 56);
            this.BtnEliminar.TabIndex = 10;
            this.BtnEliminar.Text = "Eliminar Cliente";
            this.BtnEliminar.UseSelectable = true;
            this.BtnEliminar.UseStyleColors = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.AccessibleName = "Botón Buscar RUT del Cliente";
            this.BtnBuscar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnBuscar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnBuscar.Location = new System.Drawing.Point(456, 168);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(56, 32);
            this.BtnBuscar.TabIndex = 2;
            this.BtnBuscar.Text = "Buscar";
            this.BtnBuscar.UseSelectable = true;
            this.BtnBuscar.UseStyleColors = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // BtnListar
            // 
            this.BtnListar.AccessibleName = "Botón para Abrir Listado de Clientes";
            this.BtnListar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnListar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnListar.Location = new System.Drawing.Point(552, 168);
            this.BtnListar.Name = "BtnListar";
            this.BtnListar.Size = new System.Drawing.Size(56, 32);
            this.BtnListar.TabIndex = 3;
            this.BtnListar.Text = "Listado";
            this.BtnListar.UseSelectable = true;
            this.BtnListar.UseStyleColors = true;
            this.BtnListar.Click += new System.EventHandler(this.BtnListar_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(160, 144);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(99, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "RUT del Cliente";
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(160, 208);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(64, 19);
            this.metroLabel2.TabIndex = 18;
            this.metroLabel2.Text = "Nombres";
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(456, 208);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(63, 19);
            this.metroLabel3.TabIndex = 19;
            this.metroLabel3.Text = "Apellidos";
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(160, 272);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(133, 19);
            this.metroLabel4.TabIndex = 20;
            this.metroLabel4.Text = "Fecha de Nacimiento";
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(456, 272);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(37, 19);
            this.metroLabel5.TabIndex = 21;
            this.metroLabel5.Text = "Sexo";
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(160, 336);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(76, 19);
            this.metroLabel6.TabIndex = 22;
            this.metroLabel6.Text = "Estado Civil";
            // 
            // AgregarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.BtnListar);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.BtnEliminar);
            this.Controls.Add(this.BtnIngresar);
            this.Controls.Add(this.ComboEstCivil);
            this.Controls.Add(this.ComboSexo);
            this.Controls.Add(this.TextApellidos);
            this.Controls.Add(this.TimePickerNacimiento);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.TextNombres);
            this.Controls.Add(this.TextRut);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AgregarCliente";
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "Ingreso de Clientes";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox TextRut;
        private MetroFramework.Controls.MetroTextBox TextNombres;
        private MetroFramework.Controls.MetroButton BtnSalir;
        private MetroFramework.Controls.MetroDateTime TimePickerNacimiento;
        private MetroFramework.Controls.MetroTextBox TextApellidos;
        private MetroFramework.Controls.MetroComboBox ComboSexo;
        private MetroFramework.Controls.MetroComboBox ComboEstCivil;
        private MetroFramework.Controls.MetroButton BtnEliminar;
        private MetroFramework.Controls.MetroButton BtnIngresar;
        private MetroFramework.Controls.MetroButton BtnListar;
        private MetroFramework.Controls.MetroButton BtnBuscar;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
    }
}