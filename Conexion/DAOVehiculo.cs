﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOVehiculo
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteVehiculo(Patente)
        public Boolean ExisteVehiculo(String Patente)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(Patente) from vehiculo where Patente = :Patente"
            };
            Comando.Parameters.Add(new OracleParameter("Patente", Patente));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region AgregarVehiculo(Patente, IdMarca, IdModelo, Anno)
        public void AgregarVehiculo(String Patente, Int32 IdMarca, Int32 IdModelo, Int32 Anno)
        {
            if (ExisteVehiculo(Patente)) { return; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "insert into vehiculo values (:Patente, :IdMarca, :IdModelo, :Anno)"
            };
            Comando.Parameters.Add(new OracleParameter("Patente", Patente));
            Comando.Parameters.Add(new OracleParameter("IdMarca", IdMarca));
            Comando.Parameters.Add(new OracleParameter("IdModelo", IdModelo));
            Comando.Parameters.Add(new OracleParameter("Anno", Anno));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion

        #region ActualizarVehiculo(Patente, IdMarca, IdModelo, Anno)
        public void ActualizarVehiculo(String Patente, Int32 IdMarca, Int32 IdModelo, Int32 Anno)
        {
            if (!ExisteVehiculo(Patente)) { return; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "update set IdMarca = :IdMarca, IdModelo = :IdModelo, Anno = :Anno where Parente = :Patente"
            };
            Comando.Parameters.Add(new OracleParameter("IdMarca", IdMarca));
            Comando.Parameters.Add(new OracleParameter("IdModelo", IdModelo));
            Comando.Parameters.Add(new OracleParameter("Anno", Anno));
            Comando.Parameters.Add(new OracleParameter("Patente", Patente));
            Comando.ExecuteNonQuery();

            Comando.Dispose();
            Conexion.CloseConexion();
        }
        #endregion

        #region GetVehiculo(Patente)
        public Vehiculo GetVehiculo(String Patente)
        {
            if (!ExisteVehiculo(Patente)) { return new Vehiculo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdMarca, IdModelo, Anno from vehiculo where Patente = :Patente"
            };
            Comando.Parameters.Add(new OracleParameter("Patente", Patente));
            OracleDataReader Reader = Comando.ExecuteReader();

            Vehiculo Result = new Vehiculo();
            while (Reader.Read())
            {
                Result = new Vehiculo
                {
                    Patente = Patente,
                    IdMarca = Convert.ToInt32(Reader["IdMarca"]),
                    IdModelo = Convert.ToInt32(Reader["IdModelo"]),
                    Anno = Convert.ToInt32(Reader["Anno"])
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetAllVehiculos()
        public List<Vehiculo> GetAllVehiculos()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Patente, IdMarca, IdModelo, Anno from vehiculo"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Vehiculo> Result = new List<Vehiculo>();
            while (Reader.Read())
            {
                Vehiculo v = new Vehiculo
                {
                    Patente = Reader["Patente"].ToString(),
                    IdMarca = Convert.ToInt32(Reader["IdMarca"]),
                    IdModelo = Convert.ToInt32(Reader["IdModelo"]),
                    Anno = Convert.ToInt32(Reader["Anno"])
                };
                Result.Add(v);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}