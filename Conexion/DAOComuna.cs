﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOComuna
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteComuna(IdComuna)
        public Boolean ExisteComuna(Int32 IdComuna)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdComuna) from comuna where IdComuna = :IdComuna"
            };
            Comando.Parameters.Add(new OracleParameter("IdComuna", IdComuna));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteComuna(Descripcion)
        public Boolean ExisteComuna(String NombreComuna)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(idComuna) from comuna where nombreComuna = :nombreComuna"
            };
            Comando.Parameters.Add(new OracleParameter("nombreComuna", NombreComuna));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetComuna(IdComuna)
        public Comuna GetComuna(Int32 IdComuna)
        {
            if (!ExisteComuna(IdComuna)) { return new Comuna(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select nombreComuna from comuna where IdComuna = :IdComuna"
            };
            Comando.Parameters.Add(new OracleParameter("IdComuna", IdComuna));
            OracleDataReader Reader = Comando.ExecuteReader();

            Comuna Result = null;
            while (Reader.Read())
            {
                Result = new Comuna
                {
                    IdComuna = IdComuna,
                    NombreComuna = Reader["nombreComuna"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetComuna(NombreComuna)
        public Comuna GetComuna(String NombreComuna)
        {
            if (!ExisteComuna(NombreComuna)) { return new Comuna(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select idComuna from comuna where nombreComuna = :nombreComuna"
            };
            Comando.Parameters.Add(new OracleParameter("nombreComuna", NombreComuna));
            OracleDataReader Reader = Comando.ExecuteReader();

            Comuna Result = null;
            while (Reader.Read())
            {
                Result = new Comuna
                {
                    IdComuna = Convert.ToInt32(Reader["idComuna"]),
                    NombreComuna = NombreComuna
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetComunas(Int32 IdRegion)
        public List<Comuna> GetComunas(Int32 IdRegion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select co.idComuna, nombreComuna from comuna co join regioncomuna rc on co.idcomuna = rc.idcomuna where idRegion = :idRegion"
            };
            Comando.Parameters.Add(new OracleParameter("idRegion", IdRegion));
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Comuna> Result = new List<Comuna>();
            while (Reader.Read())
            {
                Comuna c = new Comuna
                {
                    IdComuna = Convert.ToInt32(Reader["idComuna"]),
                    NombreComuna = Reader["nombreComuna"].ToString()
                };
                Result.Add(c);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}