﻿using System;

namespace Modelo.Clases
{
    public class RegionComuna
    {
        public Int32 IdRegion { get; set; }
        public Int32 IdComuna { get; set; }
    }
}