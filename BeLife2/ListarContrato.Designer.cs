﻿namespace Vista
{
    partial class ListarContrato
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListarContrato));
            this.GridLista = new MetroFramework.Controls.MetroGrid();
            this.BtnSalir = new MetroFramework.Controls.MetroButton();
            this.ComboModo = new MetroFramework.Controls.MetroComboBox();
            this.ComboFiltro = new MetroFramework.Controls.MetroComboBox();
            this.BtnCargar = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoContrato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vigente = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iniciovigencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finvigencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.declaracion = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.primaanual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.primamensual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.observacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridLista)).BeginInit();
            this.SuspendLayout();
            // 
            // GridLista
            // 
            this.GridLista.AccessibleName = "Lista de Resultados de Contratos";
            this.GridLista.AllowUserToAddRows = false;
            this.GridLista.AllowUserToDeleteRows = false;
            this.GridLista.AllowUserToResizeColumns = false;
            this.GridLista.AllowUserToResizeRows = false;
            this.GridLista.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GridLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GridLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GridLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.GridLista.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numero,
            this.creacion,
            this.termino,
            this.rut,
            this.tipoContrato,
            this.plan,
            this.vigente,
            this.iniciovigencia,
            this.finvigencia,
            this.declaracion,
            this.primaanual,
            this.primamensual,
            this.observacion});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridLista.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridLista.EnableHeadersVisualStyles = false;
            this.GridLista.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.GridLista.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.GridLista.Location = new System.Drawing.Point(48, 192);
            this.GridLista.MultiSelect = false;
            this.GridLista.Name = "GridLista";
            this.GridLista.ReadOnly = true;
            this.GridLista.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridLista.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridLista.RowHeadersWidth = 20;
            this.GridLista.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridLista.Size = new System.Drawing.Size(744, 352);
            this.GridLista.StandardTab = true;
            this.GridLista.TabIndex = 3;
            this.GridLista.UseStyleColors = true;
            // 
            // BtnSalir
            // 
            this.BtnSalir.AccessibleName = "Botón Volver";
            this.BtnSalir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnSalir.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnSalir.Location = new System.Drawing.Point(48, 64);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(69, 40);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.Text = "Volver";
            this.BtnSalir.UseSelectable = true;
            this.BtnSalir.UseStyleColors = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // ComboModo
            // 
            this.ComboModo.AccessibleName = "Menú Selección Modo de Busqueda";
            this.ComboModo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboModo.FormattingEnabled = true;
            this.ComboModo.ItemHeight = 23;
            this.ComboModo.Location = new System.Drawing.Point(48, 136);
            this.ComboModo.Name = "ComboModo";
            this.ComboModo.Size = new System.Drawing.Size(224, 29);
            this.ComboModo.TabIndex = 1;
            this.ComboModo.UseSelectable = true;
            this.ComboModo.SelectedIndexChanged += new System.EventHandler(this.ComboModo_SelectedIndexChanged);
            // 
            // ComboFiltro
            // 
            this.ComboFiltro.AccessibleName = "Menú Selección Filtro a Aplicar";
            this.ComboFiltro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboFiltro.FormattingEnabled = true;
            this.ComboFiltro.ItemHeight = 23;
            this.ComboFiltro.Location = new System.Drawing.Point(568, 136);
            this.ComboFiltro.Name = "ComboFiltro";
            this.ComboFiltro.Size = new System.Drawing.Size(224, 29);
            this.ComboFiltro.TabIndex = 2;
            this.ComboFiltro.UseSelectable = true;
            this.ComboFiltro.SelectedIndexChanged += new System.EventHandler(this.ComboFiltro_SelectedIndexChanged);
            // 
            // BtnCargar
            // 
            this.BtnCargar.AccessibleName = "Botón Cargar Contrato";
            this.BtnCargar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BtnCargar.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.BtnCargar.Location = new System.Drawing.Point(312, 136);
            this.BtnCargar.Name = "BtnCargar";
            this.BtnCargar.Size = new System.Drawing.Size(224, 29);
            this.BtnCargar.TabIndex = 4;
            this.BtnCargar.Text = "Cargar Contrato";
            this.BtnCargar.UseSelectable = true;
            this.BtnCargar.UseStyleColors = true;
            this.BtnCargar.Click += new System.EventHandler(this.BtnCargar_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(48, 112);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 19);
            this.metroLabel1.TabIndex = 6;
            this.metroLabel1.Text = "Modo de Busqueda";
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(568, 112);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(95, 19);
            this.metroLabel2.TabIndex = 7;
            this.metroLabel2.Text = "Filtro a Aplicar";
            // 
            // numero
            // 
            this.numero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.numero.HeaderText = "Numero Contrato";
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            this.numero.Width = 110;
            // 
            // creacion
            // 
            this.creacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.creacion.HeaderText = "Fecha Creacion";
            this.creacion.Name = "creacion";
            this.creacion.ReadOnly = true;
            this.creacion.Width = 99;
            // 
            // termino
            // 
            this.termino.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.termino.HeaderText = "Fecha Termino";
            this.termino.Name = "termino";
            this.termino.ReadOnly = true;
            this.termino.Width = 94;
            // 
            // rut
            // 
            this.rut.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.rut.HeaderText = "Rut Cliente";
            this.rut.Name = "rut";
            this.rut.ReadOnly = true;
            this.rut.Width = 80;
            // 
            // tipoContrato
            // 
            this.tipoContrato.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tipoContrato.HeaderText = "Tipo Contrato";
            this.tipoContrato.Name = "tipoContrato";
            this.tipoContrato.ReadOnly = true;
            this.tipoContrato.Width = 93;
            // 
            // plan
            // 
            this.plan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.plan.HeaderText = "Nombre Plan";
            this.plan.Name = "plan";
            this.plan.ReadOnly = true;
            this.plan.Width = 88;
            // 
            // vigente
            // 
            this.vigente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.vigente.HeaderText = "Contrato Vigente";
            this.vigente.Name = "vigente";
            this.vigente.ReadOnly = true;
            this.vigente.Width = 90;
            // 
            // iniciovigencia
            // 
            this.iniciovigencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.iniciovigencia.HeaderText = "Inicio Vigencia";
            this.iniciovigencia.Name = "iniciovigencia";
            this.iniciovigencia.ReadOnly = true;
            this.iniciovigencia.Width = 96;
            // 
            // finvigencia
            // 
            this.finvigencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.finvigencia.HeaderText = "Fin Vigencia";
            this.finvigencia.Name = "finvigencia";
            this.finvigencia.ReadOnly = true;
            this.finvigencia.Width = 85;
            // 
            // declaracion
            // 
            this.declaracion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.declaracion.HeaderText = "Declaracion Salud";
            this.declaracion.Name = "declaracion";
            this.declaracion.ReadOnly = true;
            this.declaracion.Width = 93;
            // 
            // primaanual
            // 
            this.primaanual.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.primaanual.HeaderText = "Prima Anual";
            this.primaanual.Name = "primaanual";
            this.primaanual.ReadOnly = true;
            this.primaanual.Width = 84;
            // 
            // primamensual
            // 
            this.primamensual.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.primamensual.HeaderText = "Prima Mensual";
            this.primamensual.Name = "primamensual";
            this.primamensual.ReadOnly = true;
            this.primamensual.Width = 96;
            // 
            // observacion
            // 
            this.observacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.observacion.HeaderText = "Observaciones";
            this.observacion.Name = "observacion";
            this.observacion.ReadOnly = true;
            this.observacion.Width = 105;
            // 
            // ListarContrato
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 580);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.BtnCargar);
            this.Controls.Add(this.ComboFiltro);
            this.Controls.Add(this.ComboModo);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.GridLista);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ListarContrato";
            this.Text = "Listado de Contratos";
            ((System.ComponentModel.ISupportInitialize)(this.GridLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid GridLista;
        private MetroFramework.Controls.MetroButton BtnSalir;
        private MetroFramework.Controls.MetroComboBox ComboModo;
        private MetroFramework.Controls.MetroComboBox ComboFiltro;
        private MetroFramework.Controls.MetroButton BtnCargar;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn creacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn termino;
        private System.Windows.Forms.DataGridViewTextBoxColumn rut;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoContrato;
        private System.Windows.Forms.DataGridViewTextBoxColumn plan;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vigente;
        private System.Windows.Forms.DataGridViewTextBoxColumn iniciovigencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn finvigencia;
        private System.Windows.Forms.DataGridViewCheckBoxColumn declaracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn primaanual;
        private System.Windows.Forms.DataGridViewTextBoxColumn primamensual;
        private System.Windows.Forms.DataGridViewTextBoxColumn observacion;
    }
}