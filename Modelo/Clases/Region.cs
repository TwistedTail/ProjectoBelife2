﻿using System;

namespace Modelo.Clases
{
    public class Region
    {
        public Int32 IdRegion { get; set; }
        public String NombreRegion { get; set; }
    }
}