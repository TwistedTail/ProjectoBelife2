﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOPlan
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExistePlan(IdPlan)
        public Boolean ExistePlan(String IdPlan)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdPlan) from plan where IdPlan = :IdPlan"
            };
            Comando.Parameters.Add(new OracleParameter("IdPlan", IdPlan));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteNombrePlan(Nombre)
        public Boolean ExisteNombrePlan(String Nombre)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdPlan) from plan where Nombre = :Nombre"
            };
            Comando.Parameters.Add(new OracleParameter("Nombre", Nombre));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetAllPlanes()
        public List<Plan> GetAllPlanes()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdPlan, Nombre, IdTipoContrato, PrimaBase, PolizaActual from plan"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Plan> Result = new List<Plan>();
            while (Reader.Read())
            {
                Plan p = new Plan
                {
                    IdPlan = Reader["IdPlan"].ToString(),
                    Nombre = Reader["Nombre"].ToString(),
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    PrimaBase = Convert.ToDouble(Reader["PrimaBase"]),
                    PolizaActual = Reader["PolizaActual"].ToString()
                };
                Result.Add(p);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetPlanesTipo(IdTipoContrato)
        public List<Plan> GetPlanesTipo(Int32 IdTipoContrato)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdPlan, Nombre, PrimaBase, PolizaActual from plan where IdTipoContrato = :IdTipoContrato"
            };
            Comando.Parameters.Add(new OracleParameter("IdTipoContrato", IdTipoContrato));
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Plan> Result = new List<Plan>();
            while (Reader.Read())
            {
                Plan P = new Plan
                {
                    IdPlan = Reader["IdPlan"].ToString(),
                    Nombre = Reader["Nombre"].ToString(),
                    IdTipoContrato = IdTipoContrato,
                    PrimaBase = Convert.ToDouble(Reader["PrimaBase"]),
                    PolizaActual = Reader["PolizaActual"].ToString()
                };
                Result.Add(P);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetPlan(IdPlan)
        public Plan GetPlan(String IdPlan)
        {
            if (!ExistePlan(IdPlan)) { return new Plan(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Nombre, IdTipoContrato, PrimaBase, PolizaActual from plan where IdPlan = :IdPlan"
            };
            Comando.Parameters.Add(new OracleParameter("IdPlan", IdPlan));
            OracleDataReader Reader = Comando.ExecuteReader();

            Plan Result = null;
            while (Reader.Read())
            {
                Result = new Plan
                {
                    IdPlan = IdPlan,
                    Nombre = Reader["Nombre"].ToString(),
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    PrimaBase = Convert.ToDouble(Reader["PrimaBase"]),
                    PolizaActual = Reader["PolizaActual"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetPlanNombre(Nombre)
        public Plan GetPlanNombre(String Nombre)
        {
            if (!ExisteNombrePlan(Nombre)) { return new Plan() { IdPlan = "" }; }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdPlan, IdTipoContrato, PrimaBase, PolizaActual from plan where Nombre = :Nombre"
            };
            Comando.Parameters.Add(new OracleParameter("Nombre", Nombre));
            OracleDataReader Reader = Comando.ExecuteReader();

            Plan Result = null;
            while (Reader.Read())
            {
                Result = new Plan
                {
                    IdPlan = Reader["IdPlan"].ToString(),
                    Nombre = Nombre,
                    IdTipoContrato = Convert.ToInt32(Reader["IdTipoContrato"]),
                    PrimaBase = Convert.ToDouble(Reader["PrimaBase"]),
                    PolizaActual = Reader["PolizaActual"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}