﻿using System;

namespace Modelo.Clases
{
    public class TipoContrato
    {
        public Int32 IdTipoContrato { get; set; }
        public String Descripcion { get; set; }
    }
}