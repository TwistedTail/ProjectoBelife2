﻿using Modelo.Clases;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;

namespace Conexion
{
    public class DAOSexo
    {
        protected ConexionDB Conexion;
        protected OracleCommand Comando;

        #region ExisteSexo(IdSexo)
        public Boolean ExisteSexo(Int32 IdSexo)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdSexo) from sexo where IdSexo = :IdSexo"
            };
            Comando.Parameters.Add(new OracleParameter("IdSexo", IdSexo));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region ExisteSexo(Descripcion)
        public Boolean ExisteSexo(String Descripcion)
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select count(IdSexo) from sexo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            Int32 Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConexion();

            return Count > 0;
        }
        #endregion

        #region GetAllSexos()
        public List<Sexo> GetAllSexos()
        {
            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdSexo, Descripcion from sexo"
            };
            OracleDataReader Reader = Comando.ExecuteReader();

            List<Sexo> Result = new List<Sexo>();
            while (Reader.Read())
            {
                Sexo s = new Sexo
                {
                    IdSexo = Convert.ToInt32(Reader["IdSexo"]),
                    Descripcion = Reader["Descripcion"].ToString()
                };
                Result.Add(s);
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetSexo(IdSexo)
        public Sexo GetSexo(Int32 IdSexo)
        {
            if (!ExisteSexo(IdSexo)) { return new Sexo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select Descripcion from sexo where IdSexo = :IdSexo"
            };
            Comando.Parameters.Add(new OracleParameter("IdSexo", IdSexo));
            OracleDataReader Reader = Comando.ExecuteReader();

            Sexo Result = null;
            while (Reader.Read())
            {
                Result = new Sexo
                {
                    IdSexo = IdSexo,
                    Descripcion = Reader["Descripcion"].ToString()
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion

        #region GetSexo(Descripcion)
        public Sexo GetSexo(String Descripcion)
        {
            if (!ExisteSexo(Descripcion)) { return new Sexo(); }

            Conexion = new ConexionDB();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConexion(),
                CommandText = "select IdSexo from sexo where Descripcion = :Descripcion"
            };
            Comando.Parameters.Add(new OracleParameter("Descripcion", Descripcion));
            OracleDataReader Reader = Comando.ExecuteReader();

            Sexo Result = null;
            while (Reader.Read())
            {
                Result = new Sexo
                {
                    IdSexo = Convert.ToInt32(Reader["IdSexo"]),
                    Descripcion = Descripcion
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConexion();

            return Result;
        }
        #endregion
    }
}