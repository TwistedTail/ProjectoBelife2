﻿using System;

namespace Modelo.Clases
{
    public class Sexo
    {
        public Int32 IdSexo { get; set; }
        public String Descripcion { get; set; }
    }
}